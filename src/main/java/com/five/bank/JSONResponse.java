package com.five.bank;

public class JSONResponse {
	public static String success(Object obj) {
		if (obj instanceof String) {
			return "{\"Success\" : \"" + obj + "\"}";
		} else {
			return "{\"Success\" : " + obj + "}";
		}
	}

	public static String failed(Object obj) {
		if (obj instanceof String) {
			return "{\"Failed\" : \"" + obj + "\"}";
		} else {
			return "{\"Failed\" : " + obj + "}";
		}
	}
}
