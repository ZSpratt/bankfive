package com.five.bank.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.five.bank.domain.Category;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Integer> {

}
