package com.five.bank.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.five.bank.domain.Status;

@Repository
public interface StatusRepo extends JpaRepository<Status, Integer> {

}
