package com.five.bank.dao;

import com.five.bank.domain.Customer;
import com.five.bank.domain.Card;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {
	Customer findByUsername(String username);
	Customer findById(int id);
	@Query("SELECT DISTINCT cat.category, count(t), sum(t.amount) "
			+ "FROM Customer c INNER JOIN Transaction t "
			+ "ON customer_id=c.id "
			+ "INNER JOIN Category cat "
			+ "ON category_id=cat.id "
			+ "WHERE c.id=:id "
			+ "GROUP BY cat.category")
	List<Object[]> findSpendingHistory(@Param("id") int id);

	@Query("SELECT DISTINCT cat.category, count(t), sum(t.amount) "
			+ "FROM Customer c INNER JOIN Transaction t "
			+ "ON customer_id=c.id "
			+ "INNER JOIN Category cat "
			+ "ON category_id=cat.id "
			+ "WHERE c.id=:id "
			+ "AND cat.id=:cid "
			+ "GROUP BY cat.category")
	List<Object[]> findSpendingHistoryByCategory(@Param("id") int id,@Param("cid") int categoryId);

	@Query("SELECT cd, t.merchantName, t.id, t.amount, t.date, cat.category "
			+ "FROM Customer c INNER JOIN Transaction t "
			+ "ON t.customer.id=c.id "
			+ "INNER JOIN Card cd "
			+ "ON t.card.id=cd.id "
			+ "INNER JOIN Category cat "
			+ "ON category_id=cat.id "
			+ "WHERE c.id=:id "
			)
	List<Object[]> findSpendingHistoryByTypeCategory(@Param("id") int id);

	@Query("SELECT cd.cardNumber, t.merchantName, t.id, t.amount, t.date, cat.category,cd.cardLimit "
			+ "FROM Customer c INNER JOIN Transaction t "
			+ "ON t.customer.id=c.id "
			+ "INNER JOIN Card cd "
			+ "ON t.card.id=cd.id "
			+ "INNER JOIN Category cat "
			+ "ON category_id=cat.id "
			+ "WHERE c.id=:id "
			+ "AND cat.category = 'Credit'"
			)
	List<Object[]> findPaymentHistory(@Param("id") int id);
	//get all cards payments with related dates
	@Query("SELECT cd.cardNumber, c.id, cd.billingDate, cd.billCycleStartDate, cd.billCycleEndDate, t.date "
			+ "FROM Customer c INNER JOIN Transaction t "
			+ "ON t.customer.id=c.id "
			+ "INNER JOIN Card cd "
			+ "ON t.card.id=cd.id "
			+ "INNER JOIN Category cat "
			+ "ON category_id=cat.id "
			+ "AND cat.category = 'Credit'")
	List<Object[]> findAllPaymentHistory();

	@Query("SELECT cd "
			+ "FROM Customer c INNER JOIN Card cd "
			+ "ON c.id=cd.customer.id "
			+ "WHERE cd.customer.id=:id "
			+ "ORDER BY cd.dateOpened")
	List<Card> findCardsWithLimit(@Param("id")int id);

	@Query("SELECT cd, c.id, c.classification "
			+ "FROM Customer c INNER JOIN Card cd "
			+ "ON c.id=customer_id "
			+ "WHERE c.id=customer_id")
	List<Card> findAllCards();

	@Query("SELECT c FROM Customer c INNER JOIN Card cd "
			+ "ON c.id=customer_id "
			+ "WHERE cd.cardNumber=:cardNo")
	Customer findCustomerByCard(@Param("cardNo") String cardNo);

	//update classification based on payment
	@Transactional
	@Modifying
	@Query("UPDATE Customer c SET classification_id=:classification WHERE c.id=:id")
	void updateClassification(@Param("id")Integer id,@Param("classification") int currentClass);

	@Query("SELECT cl FROM Classification cl WHERE id=:id")
	String findClassification(@Param("id")int currentClass);

	@Query("SELECT c FROM Card c WHERE customer_id=:id AND cardNumber=:cardNo")
	Card findCardByCustomer(@Param("id")Integer id,@Param("cardNo") String cardNo);

	@Query("SELECT c "
			+ "FROM Customer c INNER JOIN Classification cl "
			+ "ON classification_id=cl.id "
			+ "WHERE cl.id=classification_id "
			+ "ORDER BY classification_id")
	List<Customer> findAllCustomerClassification();

	//get all customer demographics as an object list
	@Query("SELECT c.age, c.gender, c.state, c.city, c.profession, c.classification "
			+ "FROM Customer c INNER JOIN Classification cl ON classification_id=cl.id "
			+ "INNER JOIN Profession p ON profession_id=p.id")
	List<Object[]> findAllCustomerDemographics();
	
	//(with customer info) get all customer demographics as an customer list
	@Query("SELECT c.age, c.gender, c.state, c.city, c.profession, c.classification, c "
			+ "FROM Customer c INNER JOIN Classification cl ON classification_id=cl.id "
			+ "INNER JOIN Profession p ON profession_id=p.id")
	List<Object[]> findAllCustomerDemographicsList();
}
