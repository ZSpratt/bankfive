package com.five.bank.dao;

import com.five.bank.domain.Loan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanRepo extends JpaRepository<Loan, Integer> {

	@Query("from Loan where id=:id")
	Loan getById(@Param("id") int id);
}
