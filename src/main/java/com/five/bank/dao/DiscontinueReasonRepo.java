package com.five.bank.dao;

import com.five.bank.domain.DiscontinueReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscontinueReasonRepo extends JpaRepository<DiscontinueReason, Integer> {
}
