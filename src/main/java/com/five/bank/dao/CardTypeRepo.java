package com.five.bank.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.five.bank.domain.CardType;

@Repository
public interface CardTypeRepo extends JpaRepository<CardType,Integer> {
	
}
