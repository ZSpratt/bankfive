package com.five.bank.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.five.bank.domain.Transaction;

@Repository
public interface TransactionRepo extends JpaRepository<Transaction,Integer> {

	@Query("FROM Transaction t INNER JOIN Category c ON category_id=c.id WHERE c.category=:category ")
	List<Transaction> findByCategoryName(@Param("category") String category);

	@Query("FROM Transaction t WHERE transaction_type=:type")
	List<Transaction> findByType(@Param("type")String type);

	@Query("SELECT DISTINCT cat.category, count(t), sum(t.amount) "
			+ "FROM Transaction t INNER JOIN Category cat "
			+ "ON category_id=cat.id "
			+ "GROUP BY cat.category")
	List<Object[]> findAllByCategoryAndCount();

	//get transactions by card number
	@Query(value = "SELECT t FROM Transaction t INNER JOIN Card c on c.id = card_id WHERE c.cardNumber = :cardNumber AND t.category.category != 'credit' ORDER BY date")
	List<Transaction> findByCardId(@Param("cardNumber") String cardNumber);


	@Query(value = "from Transaction t inner join Customer customer on customer_id = customer.id where category_id = 0 and customer.id = :id group by on_time")
	List<Transaction> findCreditByCustomer(@Param("id") int id);

	// find Region wise usage of Credit card
	@Query(value = "SELECT DISTINCT state FROM Transaction t WHERE card_id IS NOT NULL AND t.card.cardNumber=:cardNumber")
	Set<String> findRegions(@Param("cardNumber") String cardNumber);

	@Query("from Transaction t where id=:id")
	Transaction getById(@Param("id") Integer id);
}
