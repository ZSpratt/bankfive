package com.five.bank.dao;

import com.five.bank.domain.Application;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepo extends JpaRepository<Application, Integer> {

	@Query("FROM Application a INNER JOIN Customer c ON applicant_id=c.id INNER JOIN Status s ON status_id=s.id WHERE s.status=:status")
	List<Application> findAllByStatus(@Param("status") String status);

	@Query("FROM Application a INNER JOIN Customer c ON applicant_id=c.id WHERE applicant_id=:id")
	List<Application> findByApplicant(@Param("id")Integer id);

	//get status by region and profession
	@Query("FROM Application a INNER JOIN Status s ON status_id=s.id INNER JOIN Customer c ON applicant_id=c.id INNER JOIN Profession p ON profession_id=p.id WHERE s.status like %:status% and c.state like %:state% and c.city like %:city% and p.profession like %:profession%")
	List<Application> findByStatusAndRegionOrProfession(@Param("status") String status, @Param("state") String state, @Param("city") String city, @Param("profession") String profession);
    
	//get applicants in date range
	@Query("SELECT DISTINCT a FROM Application a INNER JOIN Customer c ON applicant_id=c.id WHERE date_opened BETWEEN :start AND :end")
	List<Application> findAllByDateRange(@Param("start")Timestamp start,@Param("end") Timestamp end);

	//Get Applications by application type
	@Query("FROM Application a where application_type like :type%")
	List<Application> findAllByApplicationType(@Param("type") String type);

	//Count Applications by type group by day
	@Query(value = "select date_format(date_opened, ' %Y-%m-%d '), count(*) from Application a where application_type like :type% group by date_format(date_opened, ' %Y-%m-%d ') order by date_opened", nativeQuery = true)
	List<Object[]> countLoanApplicationsByDay(@Param("type") String type);

	//Count Applications by type group by month
	@Query(value = "select date_format(date_opened, ' %Y-%m '), count(*) from Application a where application_type like :type% group by date_format(date_opened, ' %Y-%m ') order by date_opened", nativeQuery = true)
	List<Object[]> countLoanApplicationsByMonth(@Param("type") String type);

	//Count Applications by type group by year
	@Query(value = "select date_format(date_opened, ' %Y '), count(*) from Application a where application_type like :type% group by date_format(date_opened, ' %Y ') order by date_opened", nativeQuery = true)
	List<Object[]> countLoanApplicationsByYear(@Param("type") String type);

	//Count Applications by type group by week
	@Query(value = "select SUBDATE(date_opened, dayofweek(date_opened) - 1), count(*) from Application a where application_type like :type% group by SUBDATE(date_opened, dayofweek(date_opened) - 1) order by date_opened", nativeQuery = true)
	List<Object[]> countLoanApplicationsByWeek(@Param("type") String type);

	//get all applications that were closed (status 3 or 4)
	@Query("FROM Application a INNER JOIN Customer c ON applicant_id=c.id INNER JOIN Status s ON status_id=s.id "
			+ "WHERE s.id=3 OR s.id=4 "
			+ "AND date_closed IS NOT NULL ")
	List<Application> findAllClosedApplications();

	@Query(value = "select status_reason, count(status_reason) from Application a where status_id = 3 and status_reason is not null and application_type like '%Credit%' group by status_reason", nativeQuery = true)
	List<Object[]> deniedCardApplications();

	@Query("FROM Application a INNER JOIN Status s ON status_id=s.id INNER JOIN Customer c ON applicant_id=c.id INNER JOIN Profession p ON profession_id=p.id WHERE s.status like %:status% and c.state like %:state% and c.city like %:city% and p.profession like %:profession% and c.prospect = True")
	List<Application> findByStatusAndRegionOrProfessionProspectTrue(@Param("status") String status, @Param("state") String state, @Param("city") String city, @Param("profession") String profession);

	@Query("FROM Application a INNER JOIN Status s ON status_id=s.id INNER JOIN Customer c ON applicant_id=c.id INNER JOIN Profession p ON profession_id=p.id WHERE s.status like %:status% and c.state like %:state% and c.city like %:city% and p.profession like %:profession% and c.prospect = False")
	List<Application> findByStatusAndRegionOrProfessionProspectFalse(@Param("status") String status, @Param("state") String state, @Param("city") String city, @Param("profession") String profession);
}