package com.five.bank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.five.bank.domain.Card;

@Repository
public interface CardRepo extends JpaRepository<Card,Integer> {

	@Query("FROM Card c INNER JOIN CardType t ON c.type=t.id WHERE t.type=:cardType and t.brand=:cardBrand")
	List<Card> findByCardType(@Param("cardType") String cardType, @Param("cardBrand") String brand);

	@Query(value = "select p.profession, count(p.profession) from Card inner join Customer as c on customer_id = c.id inner join Profession as p on c.profession_id=p.id group by p.id", nativeQuery = true)
	List<Object[]> cardsByProfession();

	@Query(value = "select c.city, c.state, count(concat(c.city, ' ',  c.state)) from Card inner join Customer as c on customer_id = c.id group by c.city, c.state", nativeQuery = true)
	List<Object[]> cardsByRegion();

	@Query("from Card c where card_number = :cardNumber")
	Card findByCardNumber(@Param("cardNumber") String cardNumber);


	@Modifying
	@Transactional
	@Query("DELETE FROM Card c WHERE c=:card")
	void deleteCard(@Param("card")Card card);
	
	@Query(value = "SELECT card.id , customer.state, customer.gender, discontinue_reason.reason, profession.profession\r\n" + 
			"FROM  (((card\r\n" + 
			"INNER JOIN customer ON card.customer_id = customer.id)\r\n" + 
			"INNER JOIN discontinue_reason ON card.discontinue_reason = discontinue_reason.id)\r\n" + 
			"INNER JOIN profession ON customer.profession_id = profession.id)\r\n" + 
			"WHERE card.active = 0", nativeQuery = true)
	public List<Object[]> getDiscontinued();

	@Query(value = "Select city, state, sum(amount) from Transaction t inner join Card c on c.id = card_id where c.card_number = :cardNumber group by city, state", nativeQuery = true)
	List<Object[]> amountByRegion(@Param("cardNumber") String cardNumber);
}
