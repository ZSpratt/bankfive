package com.five.bank.dao;

import com.five.bank.domain.Classification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassificationRepo extends JpaRepository<Classification, Integer> {
}
