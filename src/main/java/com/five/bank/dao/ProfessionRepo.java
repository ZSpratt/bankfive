package com.five.bank.dao;

import com.five.bank.domain.Card;
import com.five.bank.domain.Profession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessionRepo extends JpaRepository<Profession,Integer> {

}
