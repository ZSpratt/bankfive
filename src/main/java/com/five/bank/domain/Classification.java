package com.five.bank.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Classification {

	@Id
	private int id;
	
	@Column
	private String classification;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	@Override
	public String toString() {
		return "{\"Classification\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"classification\":\"" + classification + "\""
				+ "}}";
	}
}
