package com.five.bank.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
public class Status {

	@Id
	@Column
	int id;

	@Column
	String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Status status1 = (Status) o;
		return id == status1.id &&
				Objects.equals(status, status1.status);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, status);
	}

	@Override
	public String toString() {
		return "{\"Status\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"status\":\"" + status + "\""
				+ "}}";
	}
}
