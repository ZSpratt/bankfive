package com.five.bank.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Category {
	@Id
	private Integer id;
	@Column
	private String category;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "{\"Category\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"category\":\"" + category + "\""
				+ "}}";
	}
}
