package com.five.bank.domain;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Customer")
public class Customer {

	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column
	private String username;
	@Column
	private String password;
	@Column
	private Boolean prospect;
	@Column(name = "firstName")
	private String firstName;
	@Column(name = "lastName")
	private String lastName;
	@Column
	private String email;
	@Column
	private Integer pin;
	@Column(name = "phoneNumber")
	private String phoneNumber;
	@Column(name = "addressLine")
	private String addressLine;
	@Column
	private String city;
	@Column
	private String state;
	@Column
	private String zipcode;
	@Column
	private String gender;
	@Column
	private Integer age;
	@Column
	boolean enabled;
	@Column
	String auth;

	// Foreign keys
	@ManyToOne
	@JoinColumn(name = "profession_id")
	private Profession profession;

	@ManyToOne
	@JoinColumn(name = "classification_id")
	private Classification classification;
    
	@JsonIgnore
	@OneToMany(mappedBy="customer", cascade = CascadeType.ALL)
	List<Transaction> transactions;
	
    @JsonIgnore
	@OneToMany(mappedBy="customer", cascade = CascadeType.ALL)
	List<Card> cards;
	
    @JsonIgnore
	@OneToMany(mappedBy="applicant", cascade=CascadeType.ALL)
	List<Application> applications;	
	
	public List<Transaction> getTransactions() {
		return transactions;
	}


	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}


	public List<Card> getCards() {
		return cards;
	}


	public void setCards(List<Card> cards) {
		this.cards = cards;
	}


	public Customer() {}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Boolean getProspect() {
		return prospect;
	}


	public void setProspect(Boolean prospect) {
		this.prospect = prospect;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Integer getPin() {
		return pin;
	}


	public void setPin(Integer pin) {
		this.pin = pin;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getAddressLine() {
		return addressLine;
	}


	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getZipcode() {
		return zipcode;
	}


	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public Integer getAge() {
		return age;
	}


	public void setAge(Integer age) {
		this.age = age;
	}


	public Profession getProfession() {
		return profession;
	}


	public void setProfession(Profession profession) {
		this.profession = profession;
	}


	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	@Override
	public String toString() {
		return "{\"Customer\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"username\":\"" + username + "\""
				+ ", \"password\":\"" + password + "\""
				+ ", \"prospect\":\"" + prospect + "\""
				+ ", \"firstName\":\"" + firstName + "\""
				+ ", \"lastName\":\"" + lastName + "\""
				+ ", \"email\":\"" + email + "\""
				+ ", \"pin\":\"" + pin + "\""
				+ ", \"phoneNumber\":\"" + phoneNumber + "\""
				+ ", \"addressLine\":\"" + addressLine + "\""
				+ ", \"city\":\"" + city + "\""
				+ ", \"state\":\"" + state + "\""
				+ ", \"zipcode\":\"" + zipcode + "\""
				+ ", \"gender\":\"" + gender + "\""
				+ ", \"age\":\"" + age + "\""
				+ ", \"enabled\":\"" + enabled + "\""
				+ ", \"auth\":\"" + auth + "\""
				+ ", \"profession\": \"" + ((profession != null) ? profession.getProfession() : null) + "\""
				+ ", \"classification\": \"" + ((classification != null) ? classification.getClassification() : null) + "\""
				+ "}}";
	}
}
