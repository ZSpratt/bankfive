package com.five.bank.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cardtype")
public class CardType {
	@Id
	@Column
	private Integer id;
	@Column
	private String brand;
	@Column
	private String type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "{\"CardType\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"brand\":\"" + brand + "\""
				+ ", \"type\":\"" + type + "\""
				+ "}}";
	}
}
