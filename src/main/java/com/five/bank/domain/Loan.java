package com.five.bank.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "loan")
public class Loan {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column()
	int id;

	@Column()
	Timestamp dateOpened;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "customer_id")
	Customer customer;

	@Column
	Double balance;

	@Column
	Double interest;

	@Column
	Boolean active;

	@Column
	String city;

	@Column
	String state;

	@JsonIgnore
	@OneToMany(mappedBy = "loan", orphanRemoval = true, cascade = CascadeType.ALL)
	List<Transaction> transactionList = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getDateOpened() {
		return dateOpened;
	}

	public void setDateOpened(Timestamp dateOpened) {
		this.dateOpened = dateOpened;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getInterest() {
		return interest;
	}

	public void setInterest(Double interest) {
		this.interest = interest;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<Transaction> getTransactionList() {
		return transactionList;
	}

	public void setTransactionList(List<Transaction> transactionList) {
		this.transactionList = transactionList;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Loan loan = (Loan) o;
		return id == loan.id &&
				Objects.equals(dateOpened, loan.dateOpened) &&
				Objects.equals(customer, loan.customer) &&
				Objects.equals(balance, loan.balance) &&
				Objects.equals(interest, loan.interest) &&
				Objects.equals(active, loan.active) &&
				Objects.equals(city, loan.city) &&
				Objects.equals(state, loan.state);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, dateOpened, customer, balance, interest, active, city, state);
	}

	@Override
	public String toString() {
		return "{\"Loan\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"dateOpened\":\"" + dateOpened.toString().split(" ")[0] + "\""
				+ ", \"customer\":\"" + customer.getFirstName() + " " + customer.getLastName() + "\""
				+ ", \"balance\":\"" + balance + "\""
				+ ", \"interest\":\"" + interest + "\""
				+ ", \"active\":\"" + active + "\""
				+ ", \"city\":\"" + city + "\""
				+ ", \"state\":\"" + state + "\""
				+ "}}";
	}
}
