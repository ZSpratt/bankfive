package com.five.bank.domain;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CascadeType;

@Entity
public class Transaction {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column//(name="merchantName")
	private String merchantName;
	@Column
	private Double amount;
	@Column
	private String city;
	@Column
	private String state;	
	@Column
	private Timestamp date;
	@Column
	@Basic(optional=true)
	private String description;
	@Column
	private Boolean onTime;
	//Foreign Key
	@ManyToOne
	@JoinColumn(nullable=false)
	private Category category;
	@ManyToOne
	@JoinColumn(nullable=true)
	private Card card;
	@ManyToOne
	@JoinColumn(nullable = true)
	private Loan loan;
	@ManyToOne
	@JoinColumn(nullable=false)
	private Customer customer;

	public Transaction() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Boolean getOnTime() {
		return onTime;
	}

	public void setOnTime(Boolean onTime) {
		this.onTime = onTime;
	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	@Override
	public String toString() {
		String transactionDate = date==null ? null: date.toString().split(" ")[0]; 
		String cardNo = card==null ? null : card.getCardNumberShort();
		String loanNo = loan==null ? null : String.valueOf(loan.getId());
		return "{\"Transaction\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"merchantName\":\"" + merchantName + "\""
				+ ", \"amount\":\"" + amount + "\""
				+ ", \"city\":\"" + city + "\""
				+ ", \"state\":\"" + state + "\""
				+ ", \"date\":\"" + transactionDate + "\""
				+ ", \"description\":\"" + description + "\""
				+ ", \"onTime\":\"" + onTime + "\""
				+ ", \"category\":\"" + category.getCategory() + "\""
				+ ", \"card\":\"" + cardNo + "\""
				+ ", \"loan\":\"" + loanNo+ "\""
				+ ", \"customer\":\"" + customer.getFirstName() + " " + customer.getLastName() + "\""
				+ "}}";
	}
}
