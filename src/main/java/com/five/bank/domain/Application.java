package com.five.bank.domain;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Application {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	int id;

	@Column
	String description;

	@Column
	Timestamp dateOpened;

	@Column
	Timestamp dateClosed;

	@ManyToOne
	@JoinColumn(name="applicant_id")
	Customer applicant;

	@ManyToOne
	@JoinColumn(name="status_id")
	Status status;

	@Column
	String applicationType;

	@Column
	String statusReason;

	public Application() {};
	public Application(Timestamp dateOpened, String description, String applicationType, Status status, Customer applicant) {
		this.dateOpened=dateOpened;
		this.description=description;
		this.applicationType=applicationType;
		this.applicant=applicant;
		this.status=status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getDateOpened() {
		return dateOpened;
	}

	public void setDateOpened(Timestamp dateOpened) {
		this.dateOpened = dateOpened;
	}

	public Timestamp getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(Timestamp dateClosed) {
		this.dateClosed = dateClosed;
	}

	public Customer getApplicant() {
		return applicant;
	}

	public void setApplicant(Customer applicant) {
		this.applicant = applicant;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Application that = (Application) o;
		return id == that.id &&
				Objects.equals(description, that.description) &&
				Objects.equals(dateOpened, that.dateOpened) &&
				Objects.equals(dateClosed, that.dateClosed) &&
				Objects.equals(applicant, that.applicant) &&
				Objects.equals(status, that.status) &&
				Objects.equals(applicationType, that.applicationType);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, description, dateOpened, dateClosed, applicant, status, applicationType);
	}

	@Override
	public String toString() {
		return "{\"Application\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"description\":\"" + description + "\""
				+ ", \"dateOpened\":\"" + ((dateOpened == null) ? null : dateOpened.toString().split(" ")[0]) + "\""
				+ ", \"dateClosed\":\"" + ((dateClosed == null) ? null : dateClosed.toString().split(" ")[0]) + "\""
				+ ", \"applicant\":\"" + ((applicant == null) ? null : applicant.getUsername()) + "\""
				+ ", \"status\":\"" + ((status == null) ? null : status.getStatus()) + "\""
				+ ", \"applicationType\":\"" + applicationType + "\""
				+ ", \"statusReason\":\"" + statusReason + "\""
				+ "}}";
	}
}
