package com.five.bank.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;

@Entity
@Table(name = "card")
public class Card {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer id;

	@Column()
	String cardNumberShort;

	@Column()
	Timestamp dateOpened;

	@Column()
	Double cardBalance;

	@Column()
	Double cardLimit;

	@Column()
	String securityCode;

	@Column()
	@JsonIgnore
	String cardNumber;

	@Column()
	boolean active;

	@Column()
	Timestamp expirationDate;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="type")
	CardType type;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="customer_id")
	Customer customer;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="discontinueReason")
	DiscontinueReason discontinueReason;

	@Column()
	Timestamp billingDate;

	@Column()
	Timestamp billCycleStartDate;

	@Column()
	Timestamp billCycleEndDate;

	@JsonIgnore
	@OneToMany(mappedBy = "card", orphanRemoval = true, cascade = CascadeType.ALL)
	List<Transaction> transactionList = new ArrayList<>();

	public Card() {super();}
	
	public Card(Timestamp dateOpened, double cardBalance, double cardLimit, String securityCode, String cardNumber,
			boolean active, Timestamp expirationDate, CardType type, Customer customer, Timestamp billingDate,
			Timestamp billCycleStartDate, Timestamp billCycleEndDate) {
		super();
		this.dateOpened = dateOpened;
		this.cardBalance = cardBalance;
		this.cardLimit = cardLimit;
		this.securityCode = securityCode;
		this.setCardNumber(cardNumber);
		this.active = active;
		this.expirationDate = expirationDate;
		this.type = type;
		this.customer = customer;
		this.billingDate = billingDate;
		this.billCycleStartDate = billCycleStartDate;
		this.billCycleEndDate = billCycleEndDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getDateOpened() {
		return dateOpened;
	}

	public void setDateOpened(Timestamp dateOpened) {
		this.dateOpened = dateOpened;
	}

	public Double getCardBalance() {
		return cardBalance;
	}

	public void setCardBalance(Double cardBalance) {
		this.cardBalance = cardBalance;
	}

	public Double getCardLimit() {
		return cardLimit;
	}

	public void setCardLimit(Double cardLimit) {
		this.cardLimit = cardLimit;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
		this.cardNumberShort = cardNumber.substring(cardNumber.length() - 4);
	}

	public String getCardNumberShort() {
		return cardNumberShort;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Timestamp getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Timestamp expirationDate) {
		this.expirationDate = expirationDate;
	}

	public CardType getType() {
		return type;
	}

	public void setType(CardType type) {
		this.type = type;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public DiscontinueReason getReason() {
		return discontinueReason;
	}

	public void setReason(DiscontinueReason discontinueReason) {
		this.discontinueReason = discontinueReason;
	}

	public Timestamp getBillingDate() {
		return billingDate;
	}

	public void setBillingDate(Timestamp billingDate) {
		this.billingDate = billingDate;
	}

	public Timestamp getBillCycleStartDate() {
		return billCycleStartDate;
	}

	public void setBillCycleStartDate(Timestamp billCycleStartDate) {
		this.billCycleStartDate = billCycleStartDate;
	}

	public Timestamp getBillCycleEndDate() {
		return billCycleEndDate;
	}

	public void setBillCycleEndDate(Timestamp billCycleEndDate) {
		this.billCycleEndDate = billCycleEndDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Card card = (Card) o;
		return active == card.active &&
				Objects.equals(id, card.id) &&
				Objects.equals(dateOpened, card.dateOpened) &&
				Objects.equals(cardBalance, card.cardBalance) &&
				Objects.equals(cardLimit, card.cardLimit) &&
				Objects.equals(securityCode, card.securityCode) &&
				Objects.equals(cardNumber, card.cardNumber) &&
				Objects.equals(expirationDate, card.expirationDate) &&
				Objects.equals(type, card.type) &&
				Objects.equals(customer, card.customer) &&
				Objects.equals(discontinueReason, card.discontinueReason) &&
				Objects.equals(billingDate, card.billingDate) &&
				Objects.equals(billCycleStartDate, card.billCycleStartDate) &&
				Objects.equals(billCycleEndDate, card.billCycleEndDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, dateOpened, cardBalance, cardLimit, securityCode, cardNumber, active, expirationDate, type, customer, discontinueReason, billingDate, billCycleStartDate, billCycleEndDate);
	}

	@Override
	public String toString() {
		String endCycle = billCycleEndDate==null? null: billCycleEndDate.toString().split(" ")[0]; 
		return "{\"Card\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"dateOpened\":\"" + dateOpened.toString().split(" ")[0] + "\""
				+ ", \"cardBalance\":\"" + cardBalance + "\""
				+ ", \"cardLimit\":\"" + cardLimit + "\""
				+ ", \"securityCode\":\"" + securityCode + "\""
				+ ", \"cardNumber\":\"" + cardNumberShort + "\""
				+ ", \"active\":\"" + active + "\""
				+ ", \"expirationDate\":\"" + expirationDate.toString().split(" ")[0] + "\""
				+ ", \"type\":" + ((type != null) ? "\"" + type.getBrand() + "\"" : null)
				+ ", \"customer\":\"" + customer.getFirstName() + " " + customer.getLastName() + "\""
				+ ", \"discontinueReason\":" + ((discontinueReason != null) ? "\"" + discontinueReason.getReason() + "\"" : null)
				+ ", \"billingDate\":\"" + billingDate.toString().split(" ")[0] + "\""
				+ ", \"billCycleStartDate\":\"" + billCycleStartDate.toString().split(" ")[0] + "\""
				+ ", \"billCycleEndDate\":\"" + endCycle + "\""
				+ "}}";
	}
}
