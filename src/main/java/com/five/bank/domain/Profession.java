package com.five.bank.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "profession")
public class Profession {
	@Id
	@Column(name = "id")
	Integer id;

	@Column(name = "profession")
	String profession;

	public Profession() {
	}

	public Profession(Integer id, String profession) {
		this.id = id;
		this.profession = profession;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	@Override
	public String toString() {
		return "{\"Profession\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"profession\":\"" + profession + "\""
				+ "}}";
	}
}
