package com.five.bank.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "discontinueReason")
public class DiscontinueReason {

	@Id
	@Column(name = "id")
	int id;

	@Column(name = "reason")
	String reason;

	public DiscontinueReason() {
	}

	public DiscontinueReason(int id, String reason) {
		this.id = id;
		this.reason = reason;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		DiscontinueReason that = (DiscontinueReason) o;
		return id == that.id &&
				Objects.equals(reason, that.reason);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, reason);
	}

	@Override
	public String toString() {
		return "{\"DiscontinueReason\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"reason\":\"" + reason + "\""
				+ "}}";
	}
}
