package com.five.bank;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@SpringBootApplication
public class BankApplication implements CommandLineRunner {

	static Logger logger = LogManager.getLogger(BankApplication.class);
	public static Random random = new Random();

	public static void main(String[] args) {
		SpringApplication.run(BankApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.debug("App started...");
		logger.info("Bank 504");	
	}

	public static Map<String, String> paramaterMaptoMap(Map<String, String[]> data) {
		Map<String, String> mapData = new HashMap<>();
		for (String key : data.keySet()) {
			mapData.put(key, data.get(key)[0]);
		}

		return mapData;
	}

}
