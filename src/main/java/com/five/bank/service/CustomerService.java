package com.five.bank.service;

import com.five.bank.JSONResponse;
import com.five.bank.Mapper;
import com.five.bank.dao.CardRepo;
import com.five.bank.dao.ClassificationRepo;
import com.five.bank.dao.ProfessionRepo;
import com.five.bank.dao.TransactionRepo;
import com.five.bank.domain.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.five.bank.dao.CustomerRepo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Service
public class CustomerService {
	static Logger logger = LogManager.getLogger(CustomerService.class);

	@Autowired
	CustomerRepo customerRepo;

	@Autowired
	ProfessionRepo professionRepo;

	@Autowired
	ClassificationRepo classificationRepo;

	@Autowired
	TransactionRepo transactionRepo;

	@Autowired
	CardRepo cardRepo;

	public List<Customer> customerList() {
		logger.info("Getting Customer List...");
		List<Customer> list = customerRepo.findAll();
		list.forEach(a -> logger.debug(a));
		return list;
	}

	public Customer findByUsername(String username) {
		return customerRepo.findByUsername(username);
	}

	public String save(Map<String, String> data, boolean update) {
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();

		Customer customer = null;

		if (update) {
			customer = customerRepo.findByUsername(data.get("username"));
		} else {
			customer = new Customer();
		}

		if (customer == null) {
			return JSONResponse.failed("Customer not in database");
		}

		customer.setUsername(data.getOrDefault("username", customer.getUsername()));

		String password = data.getOrDefault("password", null);
		if (password != null) {
			System.out.println("Changed Password");
			customer.setPassword(bcrypt.encode(data.get("password")));
		}

		try {
			if (data.containsKey("prospect")) {
				customer.setProspect(Boolean.valueOf(data.get("prospect")));
			} else if (!update) {
				customer.setProspect(true);
			}
		} catch (Exception e) {

		}
		customer.setFirstName(data.getOrDefault("firstname", customer.getFirstName()));
		customer.setLastName(data.getOrDefault("lastname", customer.getLastName()));
		customer.setEmail(data.getOrDefault("email", customer.getEmail()));
		try {
			String pin = data.getOrDefault("pin", null);
			if (pin == null && customer.getAge() != null) {
				pin = customer.getPin().toString();
			}
			if (pin != null) {
				customer.setPin(Integer.parseInt(pin));
			}
		} catch (Exception e) {

		}
		customer.setPhoneNumber(data.getOrDefault("phonenumber", customer.getPhoneNumber()));
		customer.setAddressLine(data.getOrDefault("address", customer.getAddressLine()));

		String city = data.getOrDefault("city", customer.getCity());
		if (city != null && city.length() > 0) {
			city = String.valueOf(city.charAt(0)).toUpperCase() + city.substring(1);
		}
		customer.setCity(city);

		String state = data.getOrDefault("state", customer.getState());
		if (state != null && state.length()>0) {
			state = state.toUpperCase();
		}
		customer.setState(state);

		customer.setZipcode(data.getOrDefault("zip", customer.getZipcode()));
		customer.setGender(data.getOrDefault("gender", customer.getGender()));
		try {
			String age = data.getOrDefault("age", null);
			if (age == null && customer.getAge() != null) {
				age = customer.getAge().toString();
			}
			if (age != null) {
				customer.setAge(Integer.parseInt(age));
			}
		} catch (Exception e) {

		}
		customer.setEnabled(true);
		customer.setAuth("ROLE_USER");

		try {
			Profession p = customer.getProfession();
			String profId = data.getOrDefault("profession_id", null);
			if (profId == null && !update) {
				p = professionRepo.getOne(0);
			}  else {
				p = professionRepo.getOne(Integer.valueOf(profId));
			}

			customer.setProfession(p);
		} catch (Exception e) {

		}

		try {
			Classification c = customer.getClassification();
			String classification_id = data.getOrDefault("classification_id", null);

			if ((classification_id == null || classification_id.equals("")) && !update) {
				c = classificationRepo.getOne(2);
			}  else {
				c = classificationRepo.getOne(Integer.valueOf(classification_id));
			}
			customer.setClassification(c);
		} catch (Exception e) {
		}

		if (!update) {
			if (customerRepo.findByUsername(customer.getUsername()) != null) {
				return JSONResponse.failed("Username in use");
			}

			if (customer.getUsername() == null || customer.getUsername().equals("")) {
				return JSONResponse.failed("Username not entered");
			}

			if (password == null || customer.getPassword().equals("")) {
				return JSONResponse.failed("Password not entered");
			}
		}

		customerRepo.save(customer);

		Customer c = customerRepo.findByUsername(data.get("username"));

		if (c != null) {
			return JSONResponse.success(customerRepo.findByUsername(c.getUsername()));
		} else {
			return JSONResponse.failed("Save Error");
		}
	}

	//get customer spending history
	public Map<String, Map<Object,Object>> getSpendHistory(int id, Integer categoryId){
		//get all spendHistory if categoryId is null
		List<Object[]> list = categoryId==null ? customerRepo.findSpendingHistory(id): customerRepo.findSpendingHistoryByCategory(id,categoryId);
		//K category, V Map<no. of transactions, sum>
		Map<String, Map<Object,Object> > transactions = new LinkedHashMap<String,Map<Object,Object> >();
		//no. of transactions and sum per transaction
		Map<Object,Object> quantitySum = new LinkedHashMap<Object,Object>();
		//quantity and sum of all transactions
		long q = 0;
		double s = 0.0;
		for(int i = 0; i<list.size();i++)
		{
		    String category = (String) list.get(i)[0];
			Object quantity = list.get(i)[1];
			Object sum = list.get(i)[2];
			
			//add to total values if transaction is not a payment("credit")
			if(!category.equals("Credit"))
			{
				q += (Long) quantity;
				s += (Double) sum;

			
			quantitySum.put("Total transactions",quantity);
			quantitySum.put("Total amount", sum);			
			transactions.put(category, quantitySum);
			}		
			quantitySum = new HashMap<Object,Object>();
		}
		//add total values
		quantitySum.put("Total transactions for customer", s);
		quantitySum.put("Total amount spent", q);
		transactions.put("Total",quantitySum);
		//log the categories
		for(Entry<String, Map<Object, Object>> entry: transactions.entrySet())
		{logger.debug("key: " + entry.getKey() + " values: " + entry.getValue());}

		return transactions;
	}

	public Map<String, Map<Object, Object>> getCardsWithLimit(int id) {
		List<Card> list = customerRepo.findCardsWithLimit(id);
		//K card number, V {card limit, card balance}
		Map<String, Map<Object,Object> > cards = new LinkedHashMap<String,Map<Object,Object> >();
		//card limit and card balance


		for(int i = 0; i<list.size();i++)
		{
		    Map<Object,Object> limitBal = new LinkedHashMap<Object,Object>();
		    String cardNo = list.get(i).getCardNumber();
		    String cardNoKey = list.get(i).getCardNumberShort();
		    limitBal.put("Card No. (Last 4-digits)", cardNoKey);
			Object limit = list.get(i).getCardLimit();
			limitBal.put("Card Limit", limit);
			//get card transactions and update card balance
			List<Transaction> transactions = transactionRepo.findByCardId(cardNo);
			double newBalance = 0.0;
			for(Transaction t: transactions)
			{
				double currentBalance = t.getAmount();
				newBalance += currentBalance;
			}
			//update card balances
			Card c = cardRepo.findByCardNumber(cardNo);
			c.setCardBalance(newBalance);
			cardRepo.save(c);
			
			limitBal.put("Card Balance", newBalance);
			cards.put(String.format("Card#%d", i+1), limitBal);
		}
		//log the cards
		for(Entry<String, Map<Object, Object>> entry: cards.entrySet())
		{logger.debug("key: " + entry.getKey() + " values: " + entry.getValue());}

		return cards;
	}
	//Vi
	//7. Credit card usage
	public Map<String, List<Map<String, Object>> > getCreditCardUsage(int id) {
		//get all spendHistory
		List<Object[]> list = customerRepo.findSpendingHistoryByTypeCategory(id);
		//K card number, V transactions info of card
		Map<String, List<Map<String, Object>> > transactions = new LinkedHashMap<String, List<Map<String, Object>> >();
		//quantity and sum of all transactions
		double total = 0.0;	

		for(int i = 0; i<list.size();i++)
		{
			Card card = (Card) list.get(i)[0];
			String cardNoKey = card.getCardNumberShort();
		    Object merchant = list.get(i)[1];
		    Object transactionId = list.get(i)[2];
		    Object amount = list.get(i)[3];
			Object date = list.get(i)[4];
			Object category = list.get(i)[5];
				
			//add to total values
			total += (Double) amount;
			
			Map<String, Object> transactionInfo =  new HashMap<>();
			
			transactionInfo.put("Merchant Name", merchant);
			transactionInfo.put("Transaction ID", transactionId);
			transactionInfo.put("Transaction amount",String.format("%.2f", amount));
			transactionInfo.put("Transaction date", date);
			transactionInfo.put("Category", category);

			List<Map<String, Object>> transactionsPerCard = transactions.get(cardNoKey)==null ? new ArrayList<>() : transactions.get(cardNoKey);	
			transactionsPerCard.add(transactionInfo);
			
			transactions.put(cardNoKey, transactionsPerCard);
//			logger.debug(transaction);
//			logger.debug(transactionsPerCard);
		}

		return transactions;
	}

	// MVP #9 find payments history with card limit
	public Map<String, Mapper> getPaymentHistory(int id) {
		//get all payment History
		List<Object[]> list = customerRepo.findPaymentHistory(id);

		Map<String, List<Object[]> > transactions = new HashMap<String, List<Object[]>>();
		//quantity and sum of all transactions
		double total = 0.0;
		List<Object[]> payment = null;
		for(int i = 0; i<list.size();i++)
		{
			String cardNo = (String)list.get(i)[0];
		    Object merchant = list.get(i)[1];
		    Object transactionId = list.get(i)[2];
		    Object amount = list.get(i)[3];
			Object date = list.get(i)[4];
			Object limit = list.get(i)[6];

			//add to total values
			total += (Double) amount;

			payment = transactions.get(cardNo);
			Object[] data = {merchant, transactionId,amount,date, limit};
			if(payment==null)
			{
			  //create new list of payment info
				payment= new ArrayList<Object[]>();
			}
			payment.add(data);
			transactions.put(cardNo, payment);
		}

		//log the categories
		for(Entry<String, List<Object[]>> entry: transactions.entrySet())
		{
			logger.debug("key: " + entry.getKey());
			for(Object[] obj : entry.getValue())
			{
				logger.debug("merchant: " + obj[0] + ", amount: " + obj[2]);
			}
		}

		HashMap<String, Mapper> output = new HashMap<>();

		for (String key : transactions.keySet()) {
			output.put(key.substring(key.length()-4), Mapper.convertToMapList(transactions.get(key), "Merchant Name", "Transaction ID", "Amount", "Date", "Limit"));
		}

		return output;
	}

	public  void updateAllCustomerClassifications() {
		List<Customer> customers = customerList();
		for (Customer customer : customers) {
			updateClassificationByPayment(customer.getId());
		}
	}

	//update classification of customers based on their payment history
	public void updateClassificationByPayment(int customer_id) {
		List<Transaction> transactions = transactionRepo.findCreditByCustomer(customer_id);
		Customer customer = customerRepo.findById(customer_id);

		float classification = 2;
		//customer has no cards
		if(customer.getCards()==null)
		{
			return;
		}
		//no payments and customer has cards
		else if(transactions.size()==0 && customer.getCards().size()>0)
			classification=0;
		else
		for (Transaction t : transactions) {
			if (t.getOnTime()) {
				classification += 0.25;
			} else {
				classification -= 0.25;
			}
		}
		int intClassification = (Math.round(classification));
		if (intClassification < 0) {
			intClassification = 0;
		} else if (intClassification > 4) {
			intClassification = 4;
		}

		customer.setClassification(classificationRepo.getOne(intClassification));
		customerRepo.save(customer);
	}

	public Map<String, Map<Object, Object>> getClassificationByPayment() {
		//K customer id, V customer classification
		Map<String, Map<Object, Object>> customerClassification = new LinkedHashMap<>();
		List<Customer> list = customerRepo.findAllCustomerClassification();
		for(Customer c: list)
		{
			//customer id, name, classification
			Map<Object,Object> info = new HashMap<>();
			info.put("customer id", c.getId());
			info.put("classification", c.getClassification().getClassification());
			info.put("name",String.format("%s %s", c.getFirstName(),c.getLastName()));
			customerClassification.put(c.getUsername(),info);
		}
		return customerClassification;
	}

	//Vi
	//15. (Without Customer info) get all customer demographics and map to with amount
	public Map<String, Map<String, Integer>> getAllCustomerDemographics() {
		//K demographic, V Map<range/specific, totalCustomers>
		Map<String, Map<String,Integer>> demographics = getDemographicMap();

		List<Object[]> list = customerRepo.findAllCustomerDemographics();


		if(list!=null)
		for(int i =0; i < list.size();i++)
		{
			//iterate through each customer/ their demographic
			for(int j = 0; j < list.get(i).length;j++)
			{
			  //specific demographic category
		      Map<String, Integer> categoryTotal = null;
		      String category = null;
		      //array reference
		      Object[] arr = list.get(i);
		      //total per category
		      int total = 0;
		      //get all category and respective values. increment total. add default value if null
		      switch(j)
			{
			//age
			case 0:
				//18-24, 25-34, 35-44, 45-54, 55-64, 65+
				int age =(int) arr[j];
				category = getAgeRange(age);
//				logger.debug(age + " " +  category);

				categoryTotal = demographics.get("age")==null ? new HashMap<>() : demographics.get("age");
				total = demographics.get("age").get(category)==null ? 0 : demographics.get("age").get(category);
				total += 1;
				categoryTotal.put(category,total);
				break;
			//gender
			case 1:
				category = (String) arr[j];
				categoryTotal = demographics.get("gender")==null ? new HashMap<>() : demographics.get("gender");
				total = demographics.get("gender").get(category)==null ? 0 : demographics.get("gender").get(category);
				total += 1;
				categoryTotal.put(category,total);

				break;
			//state
			case 2:
				//whatever states are in DB
				category = (String) arr[j];
				categoryTotal = demographics.get("state")==null ? new HashMap<>() : demographics.get("state");
				total = demographics.get("state").get(category)==null ? 0 : demographics.get("state").get(category);
				total += 1;
				categoryTotal.put(category,total);
				break;
			//city
			case 3:
				//whatever cities are in DB
				category = (String) arr[j];
				categoryTotal = demographics.get("city")==null ? new HashMap<>() : demographics.get("city");
				total = demographics.get("city").get(category)==null ? 0 : demographics.get("city").get(category);
				total += 1;
				categoryTotal.put(category,total);
				break;
			//profession
			case 4:
				Profession p = (Profession) arr[j];
				category = p.getProfession();
				categoryTotal = demographics.get("profession")==null ? new HashMap<>() : demographics.get("profession");
				total = demographics.get("profession").get(category)==null ? 0 : demographics.get("profession").get(category);
				total += 1;
				categoryTotal.put(category,total);
				break;
			//classification
			case 5:
				Classification c = (Classification) arr[j];
				category = c.getClassification();
				categoryTotal = demographics.get("classification")==null ? new HashMap<>() : demographics.get("classification");
				total = demographics.get("classification").get(category)==null ? 0 : demographics.get("classification").get(category);
				total += 1;
				categoryTotal.put(category,total);
				break;
			default: break;
			}
			}
		}

		for(Map.Entry<String, Map<String,Integer>> entry: demographics.entrySet())
		{
			logger.debug("key: " + entry.getKey() + " value: " + entry.getValue());
		}

		return demographics;
	}

	//get age range
	String getAgeRange(int age) {
		//18-24, 25-34, 35-44, 45-54, 55-64, 65+
		String range = "65+";
		if(age > 17 && age < 25 )
		{
			range = "18-24";
		}
		else if(age > 24 && age < 35 )
		{
			range = "25-34";
		}
		else if(age > 34 && age < 45 )
		{
			range = "35-44";
		}
		else if(age > 44 && age < 55 )
		{
			range = "45-54";
		}
		else if(age > 54 && age < 65 )
		{
			range = "55-64";
		}
		return range;
	}
	//return empty demographics with category map with keys filled
	Map<String, Map<String,Integer>> getDemographicMap(){
		Map<String, Integer> cat = new HashMap<>();
		Map<String, Map<String,Integer>> map = new HashMap<>();

		//age
		cat.put("18-24", null);
		cat.put("25-34", null);
		cat.put("35-44", null);
		cat.put("45-54", null);
		cat.put("55-64", null);
		cat.put("65+", null);
		map.put("age", cat);
		cat = new HashMap<>();

		cat.put("male", null);
		cat.put("female", null);
		cat.put("other", null);
		map.put("gender", cat);
		cat = new HashMap<>();

		map.put("state", new HashMap<>());
		map.put("city", new HashMap<>());
		//profession
		for(Profession p: professionRepo.findAll())
		{
			cat.put(p.getProfession(), null);
		}
		map.put("profession", cat);
		cat = new HashMap<>();
		//classification
		for(Classification c: classificationRepo.findAll())
		{
			cat.put(c.getClassification(), null);
		}
		map.put("classification", cat);
		return map;
	}

	//Vi
	//**15. (With Customer info) get all customer demographics and map to with amount
	public Map<String, Map<String, List<Object>>> getAllCustomerDemographicsList() {

		List<Object[]> list = customerRepo.findAllCustomerDemographicsList();
		//K demographic, V Map<range/specific, List of customers>
		Map<String, Map<String, List<Object>>> demographics = getDemographicMapList();

		if(list!=null)
		for(int i =0; i < list.size();i++)
		{
			//iterate through each customer/ their demographic
			for(int j = 0; j < list.get(i).length-1;j++)
			{
			  //specific demographic category
		      String category = null;
		      //array reference
		      Object[] arr = list.get(i);
		      Customer customer = (Customer) arr[6];
		      //get all category and respective values. increment total. add default value if null
		      switch(j)
			{
			//age
			case 0:
				//18-24, 25-34, 35-44, 45-54, 55-64, 65+
				int age =(int) arr[j];
				category = getAgeRange(age);
//				logger.debug(age + " " +  category);
				setCategoryTotalMap(category,"age",customer, demographics);
				break;
			//gender
			case 1:
				category = (String) arr[j];
				setCategoryTotalMap(category,"gender",customer, demographics);
				break;
			//state
			case 2:
				//whatever states are in DB
				category = (String) arr[j];
				setCategoryTotalMap(category,"state",customer, demographics);
				break;
			//city
			case 3:
				//whatever cities are in DB
				category = (String) arr[j];
				setCategoryTotalMap(category,"city",customer, demographics);
				break;
			//profession
			case 4:
				Profession p = (Profession) arr[j];
				category = p.getProfession();
				setCategoryTotalMap(category,"profession",customer, demographics);
				break;
			//classification
			case 5:
				Classification c = (Classification) arr[j];
				category = c.getClassification();
				setCategoryTotalMap(category,"classification",customer, demographics);
				break;
			default: break;
			}
			}
		}

		for(Entry<String, Map<String, List<Object>>> entry: demographics.entrySet())
		{
			logger.debug("key: " + entry.getKey() + " value: " + entry.getValue());
		}

		return demographics;
	}

	//set each individual category/ update total per category (for demographics)
	void setCategoryTotalMap(String category, String subcategory, Customer customer, Map<String, Map<String, List<Object>>> demographics ){
		Map<String, List<Object>> categoryTotal = demographics.get(subcategory)==null ? new HashMap<>() : demographics.get(subcategory);
		List<Object> categoryData = demographics.get(subcategory).get(category)==null ? new ArrayList<>() : demographics.get(subcategory).get(category);
		//obj 1 = total, obj n = customers...
		//get current total customers in the category
		String str =categoryData.size()==0 ? "0": (String) categoryData.get(0);
		int total = str.equals("0") ? 1 : Integer.parseInt(str.substring(str.length()-1,str.length())) + 1;
		//add data to map
		if(categoryData.isEmpty())
		{

			categoryData.add(0, String.format("Total Customers: %d", total));
		} else
		{
			categoryData.set(0, String.format("Total Customers: %d", total));
		}
		//then add customer to the list
		//categoryData.add(String.format("Customer id: %d, Customer Name: %s %s", customer.getId(), customer.getFirstName(), customer.getLastName()));
		categoryData.add(customer.getFirstName() + " " + customer.getLastName());
		categoryTotal.put(category,categoryData);
	}

	//(with customer info) return empty demographics with category map with keys filled
	Map<String, Map<String,List<Object>>> getDemographicMapList(){
		Map<String, List<Object>> cat = new HashMap<>();
		Map<String, Map<String,List<Object>>> map = new HashMap<>();

		//age
		cat.put("18-24", null);
		cat.put("25-34", null);
		cat.put("35-44", null);
		cat.put("45-54", null);
		cat.put("55-64", null);
		cat.put("65+", null);
		map.put("age", cat);
		cat = new HashMap<>();

		cat.put("male", null);
		cat.put("female", null);
		cat.put("other", null);		
		map.put("gender", cat);
		cat = new HashMap<>();

		map.put("state", new HashMap<>());
		map.put("city", new HashMap<>());
		//profession
		for(Profession p: professionRepo.findAll())
		{
			cat.put(p.getProfession(), null);
		}
		map.put("profession", cat);
		cat = new HashMap<>();
		//classification
		for(Classification c: classificationRepo.findAll())
		{
			cat.put(c.getClassification(), null);
		}
		map.put("classification", cat);
		return map;
	}

	public String deleteCustomer(Map<String, String> customerData) {

		try {
			Customer customer = customerRepo.getOne(Integer.valueOf(customerData.get("username")));

			if (customer != null) {
				System.out.println("Deleting Customer : " + customer);
				customerRepo.delete(customer);
			} else {
				return "{\"Failed\" : \"Customer Data Failed to Delete\"}";
			}
		} catch (Exception e) {
			try {
				Customer customer = customerRepo.findByUsername(customerData.get("username"));

				if (customer != null) {
					System.out.println("Deleting Customer : " + customer);
					customerRepo.delete(customer);
				} else {
					return "{\"Failed\" : \"Customer Data Failed to Delete\"}";
				}
			} catch (Exception ex) {
				return "{\"Failed\" : \"Customer Data Not Found\"}";
			}
		}

		return "{\"Success\" : \"Customer Data Deleted\"}";
	}

	public Customer getByUsername(String name) {
		return customerRepo.findByUsername(name);
	}
}
