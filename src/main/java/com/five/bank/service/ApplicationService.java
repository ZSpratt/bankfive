package com.five.bank.service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.five.bank.JSONResponse;
import com.five.bank.domain.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.five.bank.dao.ApplicationRepo;
import com.five.bank.dao.StatusRepo;
import com.five.bank.domain.Application;
import com.five.bank.domain.Customer;

@Service
public class ApplicationService {
	@Autowired
	ApplicationRepo repo;
	@Autowired
	CustomerService customerService;
	@Autowired
	StatusRepo statusRepo;
	static Logger logger = LogManager.getLogger(ApplicationService.class);

	public List<Application> findAll() {
		return repo.findAll();
	}

	public String save(Map<String, String> data) {

		if (!data.containsKey("username")) {
			return JSONResponse.failed("Username not supplied");
		}
		if (!data.containsKey("application_type")) {
			return JSONResponse.failed("Required Data Not Complete");
		}

		if (data.get("application_type").contains("Loan") && !data.containsKey("amount")) {
			return JSONResponse.failed("Loan does not contain amount");
		}

		String application_type = data.get("application_type");
		if (data.get("application_type").contains("Loan") && data.containsKey("amount")) {
			application_type += " " + data.get("amount");
		}

		Customer applicant = customerService.findByUsername(data.get("username"));
		Application a = null;
		if (applicant == null) {
			JSONResponse.failed("No user by username");
		}

		Timestamp dateOpened = new Timestamp(System.currentTimeMillis());
		if (data.containsKey("date_opened"))
			dateOpened = Timestamp.valueOf(data.get("date_opened"));

		a = new Application(
				dateOpened,
				data.get("application_type").split(" ")[0] + " Application for " + applicant.getFirstName(),
				application_type,
				statusRepo.findById(1).get(),
				applicant);

		repo.save(a);
		return JSONResponse.success(a);
	}

	public String update(Map<String, String> data) {
		logger.debug(data);

		if (!data.containsKey("id")) {
			return JSONResponse.failed("No ID Provided");
		}

		Optional<Application> application = repo.findById(Integer.parseInt(data.get("id")));

		if (application.isEmpty()) {
			return JSONResponse.failed("Application ID Not Found");
		}

		Application app = application.get();

		Status status = app.getStatus();
		if (data.containsKey("status")) {
			try {
				Integer statusID = Integer.valueOf(data.get("status"));
				Optional<Status> stat = statusRepo.findById(statusID);
				if (stat.isEmpty()) {
					return JSONResponse.failed("Status not found in database");
				}
				status = stat.get();
			} catch (Exception e) {
				return JSONResponse.failed("Status is not an ID");
			}

			if (data.containsKey("status_reason")) {
				app.setStatusReason(data.get("status_reason"));
			}
		}

		app.setStatus(status);

		if (status.getId() == 2 || status.getId() == 3) {
			Timestamp closedDate = new Timestamp(System.currentTimeMillis());
			app.setDateClosed(closedDate);
		} else {
			app.setDateClosed(null);
			app.setStatusReason("");
		}



		return JSONResponse.success(app);
	}

	public String deleteApplication(Map<String, String> data) {
		if (!data.containsKey("id")) {
			return JSONResponse.failed("No ID Provided");
		}

		Optional<Application> application = repo.findById(Integer.parseInt(data.get("id")));

		if (application.isEmpty()) {
			return JSONResponse.failed("Application ID Not Found");
		}

		Application app = application.get();

		if (app != null) {
			repo.delete(app);
		}
		return JSONResponse.success("Application Deleted");
	}

	public List<Application> findAllByStatus(String status) {
		return repo.findAllByStatus(status);
	}

	public List<Application> findByApplicant(Integer id) {
		return repo.findByApplicant(id);
	}

	public String findById(int id) {
		Optional<Application> a = repo.findById(id);

		if (a.isEmpty()) {
			return JSONResponse.failed("Application with ID not found");
		} else {
			return a.get().toString();
		}

	}

	//filter by status, state/city or profession
	public List<Application> filter(Map<String, String> filterData) {
		String status = filterData.getOrDefault("status", "%");
		String state = filterData.getOrDefault("state", "%");
		state = state.toUpperCase();
		String city = filterData.getOrDefault("city", "%");
		city = String.valueOf(city.charAt(0)).toUpperCase() + city.substring(1);
		String profession = filterData.getOrDefault("profession", "%");
		if (filterData.containsKey("prospect")) {
			Boolean prospect = Boolean.valueOf(filterData.get("prospect"));
			if (prospect) {
				return repo.findByStatusAndRegionOrProfessionProspectTrue(status, state, city, profession);
			}
			else {
				return repo.findByStatusAndRegionOrProfessionProspectFalse(status, state, city, profession);
			}
		}

		return repo.findByStatusAndRegionOrProfession(status, state, city, profession);
	}

	//get total applications in date range
	public Integer findTotalByDateRange(String start, String end) {
		List<Application> list = repo.findAllByDateRange(Timestamp.valueOf(start + " 00:00:00"), Timestamp.valueOf(end + " 23:59:59"));

		return list.size();
	}

	//get average processing time for applications
	public String findAverageTime() {
		List<Application> list = repo.findAllClosedApplications();
		//K status(accepted,denied)
//        Map<String, String> statusTime = new HashMap<>(); 
		long avgTime = 0;
//         logger.debug("list:"+list); 	
		if (!list.isEmpty())
			for (Application a : list) {
				logger.debug("application id: " + a.getId() + " status: " + a.getStatus().getStatus());
				Timestamp closed = a.getDateClosed();
				Timestamp open = a.getDateOpened();
				long days = TimeUnit.MILLISECONDS.toDays(Math.abs((closed.getTime() - open.getTime())));
				avgTime += days;

			}
		avgTime = avgTime / list.size();
		logger.debug("Average time for application decision (days):" + avgTime);
		return  "Average time for application decision:" + avgTime + " Days";
	}

	//get applications from a date range
	public List<Application> findByDateRange(String start, String end) {
		if (start == null) {
			start = "2020-01-01";
		}
		if (end == null) {
			end = "2025-12-31";
		}
		List<Application> list = repo.findAllByDateRange(Timestamp.valueOf(start + " 00:00:00"), Timestamp.valueOf(end + " 23:59:59"));
		list.forEach(a -> logger.debug(a));

		return list;
	}
}
