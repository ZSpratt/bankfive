package com.five.bank.service;

import com.five.bank.JSONResponse;
import com.five.bank.Mapper;
import com.five.bank.dao.ApplicationRepo;
import com.five.bank.dao.CustomerRepo;
import com.five.bank.dao.LoanRepo;
import com.five.bank.domain.Application;
import com.five.bank.domain.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

@Service
public class LoanService {

	@Autowired
	LoanRepo loanRepo;

	@Autowired
	ApplicationRepo applicationRepo;

	@Autowired
	CustomerRepo customerRepo;

	public List<Loan> findAll() {
		return loanRepo.findAll();
	}

	public List<Application> findLoanApplications() {
		return applicationRepo.findAllByApplicationType("Loan");
	}

	public Mapper countLoanApplicationsByDate(Map<String, String> data) {
		String filter = data.get("filter");

		if(filter==null)
		{
			return null;
		}

		List<Object[]> results = null;
		if (filter.equalsIgnoreCase("Day")) {
			results = applicationRepo.countLoanApplicationsByDay("Loan");
		} else if (filter.equalsIgnoreCase("Month")) {
			results = applicationRepo.countLoanApplicationsByMonth("Loan");
		} else if (filter.equalsIgnoreCase("Year")) {
			results = applicationRepo.countLoanApplicationsByYear("Loan");
		} else if (filter.equalsIgnoreCase("Week")) {
			results = applicationRepo.countLoanApplicationsByWeek("Loan");
		}

		return Mapper.convertToMapList(results, filter.toLowerCase(), "count");
	}

	public String createLoan(Map<String, String> data) {
		if (!data.containsKey("customer_id"))
			return JSONResponse.failed("No Customer ID Supplied");
		if (!data.containsKey("balance"))
			return JSONResponse.failed("No Balance Supplied");
		if (!data.containsKey("city"))
			return JSONResponse.failed("No City Supplied");
		if (!data.containsKey("state"))
			return JSONResponse.failed("No State Supplied");

		Loan loan = new Loan();
		//get today's time stamp with calendar
		Calendar cal = Calendar.getInstance();
		loan.setActive(Boolean.valueOf(data.getOrDefault("active", "True")));
		loan.setBalance(Double.valueOf(data.getOrDefault("balance", "10000")));
		loan.setCity(data.getOrDefault("city", "null").substring(0,1).toUpperCase()+data.getOrDefault("city", "null").substring(1));
		loan.setState(data.getOrDefault("state", "null").toUpperCase());
		loan.setDateOpened(new Timestamp(cal.getTime().getTime()));
		loan.setInterest(Double.valueOf(data.getOrDefault("interest", "0.05")));
		String customer_id = data.getOrDefault("customer_id", null);

		if (customer_id != null) {
			loan.setCustomer(customerRepo.findById(Integer.parseInt(customer_id)));
		} else {
			return JSONResponse.failed("No customer_id specified");
		}

		loanRepo.save(loan);
		return JSONResponse.success(loan);

	}

	public String updateLoan(Map<String, String> data) {
		String loan_id = data.getOrDefault("id", null);
		if (loan_id == null) {
			return JSONResponse.failed("No ID specified");
		}

		int id = Integer.parseInt(loan_id);
		Loan loan = null;
		loan = loanRepo.getById(id);

		if (loan == null) {
			return JSONResponse.failed("No such loan exists in db");
		}

		if (data.containsKey("active"))
			loan.setActive(Boolean.valueOf(data.get("active")));
		if (data.containsKey("balance"))
			loan.setBalance(Double.valueOf(data.get("balance")));
		if (data.containsKey("city"))
			loan.setCity(data.get("city").substring(0,1).toUpperCase()+data.get("city").substring(1));
		if (data.containsKey("state"))
			loan.setState(data.get("state").toUpperCase());
		if (data.containsKey("date_opened"))
			loan.setDateOpened(Timestamp.valueOf(data.get("date_opened")));
		if (data.containsKey("interest"))
			loan.setInterest(Double.valueOf(data.get("interest")));
		if (data.containsKey("customer_id")) {
			String customer_id = data.get("customer_id");
			if (customer_id != null) {
				loan.setCustomer(customerRepo.findById(Integer.parseInt(customer_id)));
			} else {
				return JSONResponse.failed("No customer_id specified");
			}
		}

		loanRepo.save(loan);
		return JSONResponse.success(loan);
	}

	public String deleteLoan(Map<String, String> data) {
		String loan_id = data.getOrDefault("id", null);
		if (loan_id == null) {
			return JSONResponse.failed("No ID specified");
		}

		int id = Integer.parseInt(loan_id);
		Loan loan = null;
		loan = loanRepo.getById(id);

		if (loan == null) {
			return JSONResponse.failed("No such loan exists in db");
		}

		loanRepo.delete(loan);

		return JSONResponse.success(loan);
	}

	public Loan findById(int id) {
		return loanRepo.findById(id).get();
	}
}
