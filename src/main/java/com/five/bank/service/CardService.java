package com.five.bank.service;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.*;

import com.five.bank.BankApplication;
import com.five.bank.JSONResponse;
import com.five.bank.Mapper;
import com.five.bank.dao.*;
import com.five.bank.domain.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Service
public class CardService {

	@Autowired
	CardRepo cardRepo;

	@Autowired
	CardTypeRepo typeRepo;

	@Autowired
	ApplicationRepo applicationRepo;

	@Autowired
	TransactionRepo transactionRepo;

	@Autowired
	CustomerRepo customerRepo;

	@Autowired
	DiscontinueReasonRepo discontinueReasonRepo;

	static Logger logger = LogManager.getLogger(CardService.class);

	public List<Card> getAllCards() {
		return cardRepo.findAll();
	}

	public String saveCard(Map<String, String> data) {

		if (!data.containsKey("customer_id") || !data.containsKey("type") || !data.containsKey("card_number"))
			return JSONResponse.failed("Missing Required Data");

		if (data.get("card_number").length() > 16) {
			return JSONResponse.failed("Card Number too long");
		}

		if (data.get("card_number").length() < 16) {
			return JSONResponse.failed("Card Number too short");
		}

		Customer c = customerRepo.findById(Integer.parseInt(data.get("customer_id")));
		Optional<CardType> cardType = typeRepo.findById(Integer.parseInt(data.get("type")));

		if (c == null) {
			return JSONResponse.failed("Customer not found in database");
		}

		if (cardType.isEmpty())
			return JSONResponse.failed("Card Type not found");
		CardType type = cardType.get();

		Card exists = cardRepo.findByCardNumber(data.get("card_number"));

		if (exists != null)
			return JSONResponse.failed("Card Number in already database");

		Timestamp date_opened = new Timestamp(System.currentTimeMillis());
		if (data.containsKey("date_opened"))
			date_opened = Timestamp.valueOf(data.get("date_opened") + " 00:00:00");

		LocalDateTime expire = date_opened.toLocalDateTime();
		expire = expire.plusYears(2);

		Timestamp expirationDate = Timestamp.valueOf(expire);
		if (data.containsKey("expiration_date"))
			expirationDate = Timestamp.valueOf(data.get("expiration_date") + " 00:00:00");

		LocalDateTime bill = date_opened.toLocalDateTime().withDayOfMonth(1);
		Timestamp bill_start_date = Timestamp.valueOf(bill);
		if (data.containsKey("billing_cycle_start_date"))
			bill_start_date = Timestamp.valueOf(data.get("billing_cycle_start_date") + " 00:00:00");

		Timestamp bill_end_date = Timestamp.valueOf(bill.withDayOfMonth(bill.getMonth().maxLength()));
		if (data.containsKey("billing_cycle_end_date"))
			bill_end_date = Timestamp.valueOf(data.get("billing_cycle_end_date") + " 00:00:00");

		Timestamp billing_date = Timestamp.valueOf(bill_end_date.toLocalDateTime().plusDays(5));
		if (data.containsKey("billing_date"))
			billing_date = Timestamp.valueOf(data.get("billing_date") + " 00:00:00");

		String securityCode = null;
		if (data.containsKey("security_code"))
			securityCode = data.get("security_code");
		else {
			int code = (int) (BankApplication.random.nextFloat() * 1000);
			securityCode = String.valueOf(code);
		}

		if (securityCode.length() > 3) {
			securityCode = securityCode.substring(0, 3);
		} else if (securityCode.length() < 3){
			securityCode = String.format("%3s", securityCode).replace(' ', '0');
		}

		Card card = new Card(
				date_opened,
				Double.parseDouble(data.getOrDefault("card_balance", "0")),
				Double.parseDouble(data.getOrDefault("card_limit", "5000.0")),
				securityCode,
				data.get("card_number"),
				true,
				expirationDate,
				type,
				c,
				billing_date,
				bill_start_date,
				bill_end_date
		);
		cardRepo.save(card);
		logger.debug(card);
		return JSONResponse.success(card);
	}

	public String update(Map<String, String> data) {

		if (!data.containsKey("card_number"))
			return JSONResponse.failed("Card Number not supplied");

		Card card = cardRepo.findByCardNumber(data.get("card_number"));

		if (card == null)
			return JSONResponse.failed("Card Number not in database");

		if (data.containsKey("billing_cycle_end_date"))
			card.setBillCycleEndDate(Timestamp.valueOf(data.get("billing_cycle_end_date") + " 00:00:00"));

		if (data.containsKey("billing_cycle_start_date"))
			card.setBillCycleStartDate(Timestamp.valueOf(data.get("billing_cycle_start_date") + " 00:00:00"));

		if (data.containsKey("billing_date"))
			card.setBillingDate(Timestamp.valueOf(data.get("billing_date") + " 00:00:00"));

		if (data.containsKey("card_balance"))
			card.setCardBalance(Double.valueOf(data.get("card_balance")));

		if (data.containsKey("card_limit"))
			card.setCardLimit(Double.valueOf(data.get("card_limit")));

		if (data.containsKey("discontinue_reason")) {
			Optional<DiscontinueReason> reason = discontinueReasonRepo.findById(Integer.valueOf(data.get("discontinue_reason")));
			if (!reason.isEmpty()) {
				card.setReason(reason.get());
			} else {
				card.setReason(null);
			}
		}

		card.setActive(card.getReason() == null);

		if (data.containsKey("type")) {
			CardType ct = typeRepo.getOne(Integer.valueOf(data.get("type")));
			card.setType(ct);
		}

		cardRepo.save(card);
		return JSONResponse.success(card);
	}

	public String delete(Map<String, String> data) {
		if (!data.containsKey("card_number"))
			return JSONResponse.failed("Card Number not supplied");

		Card card = cardRepo.findByCardNumber(data.get("card_number"));

		if (card == null)
			return JSONResponse.failed("Card not in database");

		cardRepo.delete(card);
		return JSONResponse.success("Card deleted");
	}

	public Card findByCardId(int id) {
		return cardRepo.findById(id).get();
	}

	public List<Card> findByCardsType(String cardType, String brand) {
		return cardRepo.findByCardType(cardType, brand);
	}

	public void save(Card c) {
		cardRepo.save(c);
	}

	public Card findById(int id) {
		return cardRepo.findById(id).get();
	}

	public List<Application> findCardApplications() {
		return applicationRepo.findAllByApplicationType("Credit");
	}

	public Mapper countCardApproval(Map<String, String> filterData) {
		String filter = filterData.get("filter");

		if (filter == null) {
			return null;
		}

		List<Object[]> results = null;
		if (filter.equals("Profession")) {
			results = cardRepo.cardsByProfession();
			return Mapper.convertToMapList(results, "profession", "count");
		} else if (filter.equals("Region")) {
			results = cardRepo.cardsByRegion();
			return Mapper.convertToMapList(results, "city", "state", "count");
		}

		return null;
	}

	public Mapper countCardApplicationsByDate(String filter) {
		String sqlFilter;

		List<Object[]> results = null;
		if (filter.equalsIgnoreCase("Day")) {
			results = applicationRepo.countLoanApplicationsByDay("Credit");
		} else if (filter.equalsIgnoreCase("Month")) {
			results = applicationRepo.countLoanApplicationsByMonth("Credit");
		} else if (filter.equalsIgnoreCase("Year")) {
			results = applicationRepo.countLoanApplicationsByYear("Credit");
		} else if (filter.equalsIgnoreCase("Week")) {
			results = applicationRepo.countLoanApplicationsByWeek("Credit");
		}

		if (results == null) {
			return  null;
		}

		return Mapper.convertToMapList(results, filter.toLowerCase(), "count");
	}

	public Mapper deniedCardApplications() {
		return Mapper.convertToMapList(applicationRepo.deniedCardApplications(), "reason", "count");
	}

	public List<Transaction> cardStatement(String cardNumber) {
		return transactionRepo.findByCardId(cardNumber);
	}

	// getting the expired cards
	public List<Card> getAllExpiredCards() {
		List<Card> cards = cardRepo.findAll();
		List<Card> expired = new ArrayList<Card>();

		LocalDate now = LocalDate.now();

		for (Card card : cards) {
			String expireDateString = card.getExpirationDate().toString();
			int year = Integer.parseInt(expireDateString.substring(0, 4));
			int month = Integer.parseInt(expireDateString.substring(5, 7));
			int day = Integer.parseInt(expireDateString.substring(8, 10));

			LocalDate expireDate = LocalDate.of(year, month, day);

			Period diff = Period.between(expireDate, now);

			if (diff.getYears() == 0 && diff.getMonths() > -3 && year >= 2021) {
				expired.add(card);
				card.setActive(false);
				cardRepo.save(card);
			}
		}
		return expired;
	}
	
	// MPP 16, get No. of credit card discontinued customer wise with region and demographics and reasons
	public Mapper getDiscontinued(){
		List<Object[]> discontinued = cardRepo.getDiscontinued();

		return Mapper.convertToMapList(discontinued, "Card ID","State","Gender", "Discontinued Reason", "Profession");
	}

	// MPP 13, get region wise usage of Credit card  
	public Mapper amountByRegion(String cardNumber) {
		Card card = cardRepo.findByCardNumber(cardNumber);
		
		if (card == null) {
			return new Mapper();
		}
		return Mapper.convertToMapList(cardRepo.amountByRegion(cardNumber), "City","State","Sum");
	}
}