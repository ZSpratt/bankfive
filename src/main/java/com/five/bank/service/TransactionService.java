package com.five.bank.service;

import java.sql.Timestamp;
import java.util.*;
import java.util.Map.Entry;

import com.five.bank.JSONResponse;
import com.five.bank.dao.CardRepo;
import com.five.bank.dao.CategoryRepo;
import com.five.bank.dao.CustomerRepo;
import com.five.bank.dao.LoanRepo;
import com.five.bank.domain.Card;
import com.five.bank.domain.Category;
import com.five.bank.domain.Customer;
import com.five.bank.domain.Loan;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.five.bank.dao.TransactionRepo;
import com.five.bank.domain.Transaction;

@Service
public class TransactionService {
	@Autowired
	TransactionRepo transactionRepo;

	@Autowired
	CardRepo cardRepo;

	@Autowired
	CategoryRepo categoryRepo;

	@Autowired
	CustomerRepo customerRepo;

	@Autowired
	LoanRepo loanRepo;
	
	static Logger logger = LogManager.getLogger(TransactionService.class);

	TransactionService() {
	}

	public List<Transaction> findAll() {
		return transactionRepo.findAll();
	}

	public List<Transaction> findByCategory(String category) {
		return transactionRepo.findByCategoryName(category);
	}

	public List<Transaction> findByType(String type) {
		return transactionRepo.findByType(type);
	}

	public Transaction findById(int id) {
		return transactionRepo.findById(id).get();
	}

	// find Region wise usage of Credit card
	public Set<String> findRegions(String cardNumber) {
		return transactionRepo.findRegions(cardNumber);
	}

	public Map<String, Map<Object, Object>> getSpendHistory() {
		List<Object[]> list = transactionRepo.findAllByCategoryAndCount();
		//K category, V Map<no. of transactions, sum>
		Map<String, Map<Object, Object>> transactions = new HashMap<String, Map<Object, Object>>();
		//no. of transactions and sum per transaction
		Map<Object, Object> quantitySum = new HashMap<Object, Object>();
		//quantity and sum of all transactions
		long q = 0;
		double s = 0.0;
		for(int i = 0; i<list.size();i++)
		{
		    String category = (String) list.get(i)[0];
			Object quantity = list.get(i)[1];
			Object sum = list.get(i)[2];
			
			//add to total values if transaction is not a payment("credit")
			if(!category.equals("Credit"))
			{
				q += (Long) quantity;
				s += (Double) sum;

			
			quantitySum.put("Total transactions",quantity);
			quantitySum.put("Total amount", sum);			
			transactions.put(category, quantitySum);
			}		
			quantitySum = new HashMap<Object,Object>();
		}
		//add total values
		quantitySum.put("Total transactions for customer", q);
		quantitySum.put("Total amount spent", s);
		transactions.put("Total transactions and amount",quantitySum);
		//log the categories
		for(Entry<String, Map<Object, Object>> entry: transactions.entrySet())
		{logger.debug("key: " + entry.getKey() + " values: " + entry.getValue());}

		return transactions;
	}

	//update card or loan balance when transaction is successful
	
	//create a transaction
	public String create(Map<String, String> data) {
		Transaction transaction = new Transaction();

		Double amount = 0.0;

		if (data.containsKey("customer_id")) {
			Integer customer_id = Integer.valueOf(data.getOrDefault("customer_id", "0"));
			Customer customer = customerRepo.findById(customer_id).get();
			transaction.setCustomer(customer);
		} else return JSONResponse.failed("No customer_id specified");
		
		if(data.containsKey("amount"))
			amount = Double.parseDouble(data.get("amount"));
		else return JSONResponse.failed("No amount specified");

		if (!data.containsKey("merchant_name"))
			return JSONResponse.failed("No Merchant Specified");
		
		transaction.setAmount(amount);
		transaction.setCity((data.getOrDefault("city", "null")));
		transaction.setState(data.getOrDefault("state", "null"));
		transaction.setDescription(data.getOrDefault("description", "null"));
		transaction.setMerchantName(data.get("merchant_name"));
		transaction.setOnTime(Boolean.valueOf(data.getOrDefault("on_time", "True")));
		
		//get today's time stamp with calendar
		Calendar cal = Calendar.getInstance();
		//if data has date, set that date, else set today's date
		Timestamp date = data.containsKey("date") ? Timestamp.valueOf(data.get("date")) :new Timestamp(cal.getTime().getTime());

		transaction.setDate(date);
		
		Integer category_id = Integer.valueOf(data.getOrDefault("category_id", "1"));
		Category category = categoryRepo.findById(category_id).get();
		transaction.setCategory(category);


		//if a card is used
		String cardNumber = data.getOrDefault("card_number", null);
		if (cardNumber != null) {
			Card card = cardRepo.findByCardNumber(cardNumber);
			//if category is a payment

			if(category.getId()==0) {
				double current = card.getCardBalance();
				if(current > 0) {
			//if payment date is after than the billing date and before the billing cycle end date 
			    //then this payment is late					
				if(date.after(card.getBillingDate()) ) {
				transaction.setOnTime(false);
				} 
				//increase balance
				card.setCardBalance(current-amount);
				} else 
					return JSONResponse.failed("No card balance due. Payment denied.");
			} else { //card is being used
				double current = card.getCardBalance();
				double limit = card.getCardLimit();
				double newAmount = current+amount;
				if(newAmount>limit)
					return JSONResponse.failed("Over card limit. Transaction denied.");
				card.setCardBalance(newAmount);				
			}
			cardRepo.save(card);
			transaction.setCard(card);
		} else {
			transaction.setCard(null);
		}
		
		//if the category is 'credit' and the payment is for a loan
		String loanId = data.getOrDefault("loan_id", null);
		if (loanId != null && category.getId()==0) {
			Loan loan= loanRepo.findById(Integer.parseInt(loanId)).get();
			double current = loan.getBalance();
			loan.setBalance(current-amount);
			loanRepo.save(loan);
			transaction.setLoan(loan);
		} else {
			transaction.setLoan(null);
		}
		
		transactionRepo.save(transaction);
		return JSONResponse.success(transaction);
	}

	//merchant name and description only
	public String update(Map<String, String> data) {
		if (!data.containsKey("id"))
			return JSONResponse.failed("No id in request");

		Integer id = Integer.valueOf(data.get("id"));

		Optional<Transaction> transactionOpt = transactionRepo.findById(id);

		if (transactionOpt.isEmpty())
			return JSONResponse.failed("Transaction not found");

		Transaction transaction = transactionOpt.get();
		if (data.containsKey("description"))
			transaction.setDescription(data.get("description"));
		if (data.containsKey("merchant_name"))
			transaction.setMerchantName(data.get("merchant_name"));
		if (data.containsKey("category_id")) {
			Optional<Category> c = categoryRepo.findById(Integer.valueOf(data.get("category_id")));
			if (c.isEmpty()) {
				return JSONResponse.failed("Category ID not found");
			}
			Category cat = c.get();
			transaction.setCategory(cat);
		}

		transactionRepo.save(transaction);

		return JSONResponse.success(transaction);
	}

	public String delete(Map<String, String> data) {
		if (!data.containsKey("id"))
			return JSONResponse.failed("No ID in request");

		Integer id = Integer.valueOf(data.get("id"));

		Optional<Transaction> transactionOpt = transactionRepo.findById(id);

		if (transactionOpt.isEmpty())
			return JSONResponse.failed("Transaction not found");

		Transaction transaction = transactionOpt.get();
		transactionRepo.delete(transaction);

		return JSONResponse.success("Transaction Deleted");
	}

	public List<Transaction> filterByCategory(String category) {
		List<Transaction> list = category==null ? null : transactionRepo.findByCategoryName(category);
		
		if(list!=null)
		list.forEach(a -> logger.debug(a));
		
		return list;
	}
}
