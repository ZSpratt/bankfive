package com.five.bank.controller;

import java.util.List;
import java.util.Map;

import com.five.bank.BankApplication;
import com.five.bank.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.five.bank.domain.Transaction;
import com.five.bank.service.TransactionService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path="/transactions", produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionController {
	static Logger logger = LogManager.getLogger(TransactionController.class);
	@Autowired
	TransactionService transactionService;

	@Autowired
	CustomerService customerService;

	@ModelAttribute
	public void updateClassifications() {
		customerService.updateAllCustomerClassifications();
	}

	@GetMapping()
	@ResponseBody
	public List<Transaction> getAllLoans() {
		logger.info("Getting all transactions...");
		List<Transaction> list = transactionService.findAll();
		list.forEach(a -> logger.debug(a));
		return list;
	}
	
	@GetMapping(value ="/{id}")
	@ResponseBody
	//get transaction by id
	public Transaction findTransactionById(@PathVariable("id") int id) {
		logger.info("Finding transaction by id...");
		Transaction t = transactionService.findById(id);
		logger.debug("result: "+ t);
		return t;
	}
	
	@GetMapping(value ="/filter")
	@ResponseBody
	//Filter transactions by specified category
	public List<Transaction> getTransactionsByCategory(@RequestParam(required=false,name="category") String category) {
		logger.info("Filtering transactions...");
		List<Transaction> list = transactionService.filterByCategory(category);
		return list;
	}

	//Vi
	//7. get all spending history for all customers
	@GetMapping("/history/spending")
	@ResponseBody
	public Map<String, Map<Object, Object>> getAllCustomerTransactions(){
		logger.info("Get Customers");
		Map<String, Map<Object, Object>> transactions = transactionService.getSpendHistory();
		return transactions;
	}
	
	@PostMapping()
	@ResponseBody
	public String insertTransaction(HttpServletRequest request) {
		logger.info("Creating transaction...");
		return transactionService.create(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

	@PutMapping()
	@ResponseBody
	//update merchant name,description
	public String updateTransaction(HttpServletRequest request) {
		logger.info("Updating transaction...");
		return transactionService.update(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}
	
	@DeleteMapping()
	@ResponseBody
	//delete transaction. Employee use.
	public String deleteTransaction(HttpServletRequest request) {
		logger.info("Deleting transaction...");
		return transactionService.delete(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

}
