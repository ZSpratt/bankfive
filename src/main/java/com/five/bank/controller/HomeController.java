package com.five.bank.controller;

import com.five.bank.dao.CustomerRepo;
import com.five.bank.domain.Customer;
import com.five.bank.service.CustomerService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
public class HomeController {
//for creating the web app GUI/routing
	static Logger logger = LogManager.getLogger(HomeController.class);
	@Autowired
	CustomerService customerService;

	//map the root page access
	@RequestMapping("/")
	public String home(Model model, Principal principal) {
		model.addAttribute("TBody", "home");
		if (principal != null) {
			model.addAttribute("Customer", customerService.getByUsername(principal.getName()));
		}
		return "index";
	}

	@RequestMapping("/customer")
	public String customer(Model model, Principal principal) {
		model.addAttribute("TBody", "customer");
		if (principal != null) {
			model.addAttribute("Customer", customerService.getByUsername(principal.getName()));
		}

		return "index";
	}

	@RequestMapping("/card")
	public String card(Model model, Principal principal) {
		model.addAttribute("TBody", "card");
		if (principal != null) {
			model.addAttribute("Customer", customerService.getByUsername(principal.getName()));
		}

		return "index";
	}

	@RequestMapping("/application")
	public String application(Model model, Principal principal) {
		model.addAttribute("TBody", "application");
		if (principal != null) {
			model.addAttribute("Customer", customerService.getByUsername(principal.getName()));
		}

		return "index";
	}

	@RequestMapping("/loan")
	public String loan(Model model, Principal principal) {
		model.addAttribute("TBody", "loan");
		if (principal != null) {
			model.addAttribute("Customer", customerService.getByUsername(principal.getName()));
		}

		return "index";
	}

	@RequestMapping("/transaction")
	public String transaction(Model model, Principal principal) {
		model.addAttribute("TBody", "transaction");
		if (principal != null) {
			model.addAttribute("Customer", customerService.getByUsername(principal.getName()));
		}

		return "index";
	}

	@RequestMapping("/mvp")
	public String mvp(Model model, Principal principal) {
		model.addAttribute("TBody", "mvp");
		if (principal != null) {
			model.addAttribute("Customer", customerService.getByUsername(principal.getName()));
		}

		return "index";
	}
}
