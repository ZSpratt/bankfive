package com.five.bank.controller;

import com.five.bank.BankApplication;
import com.five.bank.Mapper;
import com.five.bank.domain.Application;
import com.five.bank.domain.Loan;
import com.five.bank.service.CustomerService;
import com.five.bank.service.LoanService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "loans", produces = MediaType.APPLICATION_JSON_VALUE)
public class LoanController {
	static Logger logger = LogManager.getLogger(LoanController.class);
	@Autowired
	LoanService loanService;

	@Autowired
	CustomerService customerService;

	@ModelAttribute
	public void updateClassifications() {
		customerService.updateAllCustomerClassifications();
	}

	@GetMapping()
	@ResponseBody
	public List<Loan> loanList() {
		logger.info("Getting loan list...");
		List<Loan> list = loanService.findAll();
		return list;
	}

	@GetMapping(value ="/{id}")
	@ResponseBody
	//get loan by id
	public Loan findTransactionById(@PathVariable("id") int id) {
		logger.info("Finding transaction by id...");
		Loan l = loanService.findById(id);
		return l;
	}

	@GetMapping(value = "/applications")
	@ResponseBody
	public List<Application> applicationList() {
		logger.info("Getting applications for loans");
		List<Application> list = loanService.findLoanApplications();
		return list;
	}

	@GetMapping(value = "/applications/date")
	@ResponseBody
	public Mapper countApplicationsByDate(HttpServletRequest request) {
		logger.info("Getting applications for loans");
		Mapper list = loanService.countLoanApplicationsByDate(BankApplication.paramaterMaptoMap(request.getParameterMap()));
		return list;
	}

	@PostMapping()
	@ResponseBody
	public String createLoan(HttpServletRequest request) {
		logger.info("Creating new Loan");
		return loanService.createLoan(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

	@PutMapping()
	@ResponseBody
	public String updateLoan(HttpServletRequest request) {
		logger.info("Updating Loan");
		return loanService.updateLoan(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

	@DeleteMapping()
	@ResponseBody
	public String deleteLoan(HttpServletRequest request) {
		logger.info("Deleting Loan");
		return loanService.deleteLoan(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}
}
