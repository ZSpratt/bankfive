package com.five.bank.controller;

import java.util.*;

import com.five.bank.BankApplication;
import com.five.bank.Mapper;
import com.five.bank.domain.*;
import com.five.bank.service.CardService;

import com.five.bank.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path="/cards", produces = MediaType.APPLICATION_JSON_VALUE)
public class CardController {
	static Logger logger = LogManager.getLogger(CardController.class);
	@Autowired
	CardService cardService;

	@Autowired
	CustomerService customerService;

	@ModelAttribute
	public void updateClassifications() {
		customerService.updateAllCustomerClassifications();
	}

	@GetMapping
	@ResponseBody
	public List<Card> getAllCards() {
		logger.info("Getting card list...");
		List<Card> list = cardService.getAllCards();
		return list;
	}

	@PostMapping
	@ResponseBody
	public String addCard(HttpServletRequest request) {
		logger.info("Adding a card");
		return cardService.saveCard(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

	@PutMapping
	@ResponseBody
	public String updateCard(HttpServletRequest request) {
		return cardService.update(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

	@DeleteMapping
	@ResponseBody
	public String deleteCard(HttpServletRequest request) {
		return cardService.delete(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

	// get Card by type
	@GetMapping(value = "/byType")
	@ResponseBody
	public List<Card> getCardsByType(@RequestParam String type, @RequestParam String brand) {
		List<Card> cards = cardService.findByCardsType(type, brand);
		return cards;
	}

	// get card by ID
	@GetMapping(value ="/{id}")
	@ResponseBody
	public Card getCardById(@PathVariable("id") int id) {
		Card card = cardService.findByCardId(id);
		return card;
	}

	@GetMapping(value = "/approved")
	@ResponseBody
	public Mapper approvedCount(HttpServletRequest request) {
		logger.info("Getting Count of approved cards");

		return cardService.countCardApproval(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

	@GetMapping(value = "/applications")
	@ResponseBody
	public List<Application> applicationList() {
		logger.info("Getting applications for cards");
		List<Application> list = cardService.findCardApplications();
		return list;
	}

	@GetMapping(value = "/applications/date")
	@ResponseBody
	public Mapper countApplicationsByDate(HttpServletRequest request) {
		logger.info("Getting applications for cards");
		String filter = request.getParameter("filter");
		Mapper list = cardService.countCardApplicationsByDate(filter);
		return list;
	}

	@GetMapping(value = "/denied")
	@ResponseBody
	public Mapper deniedCardApplications() {
		return cardService.deniedCardApplications();
	}

	@GetMapping(value = "/{cardNumber}/statement")
	@ResponseBody
	public List<Transaction> cardStatements(@PathVariable("cardNumber") String cardNumber) {
		return cardService.cardStatement(cardNumber);
	}
	
	// Zaid
	// MPP 13, get region wise usage of Credit card  
	@GetMapping(value = "/{cardNumber}/regions")
	@ResponseBody
	public Mapper getAmountByRegion(@PathVariable("cardNumber") String cardNumber) {
		return cardService.amountByRegion(cardNumber);
	}
	
	// Zaid
	// MVP 12, Credit card expiring in next 3 months  
	@GetMapping(value = "/expired")
	@ResponseBody
	public List<Card> getAllExpiredCards() {
		logger.info("Getting card list...");
		List<Card> list = cardService.getAllExpiredCards();
		return list;
	}
	
	// Zaid
	// MVP 16, get No. of credit card discontinued customer wise with region and demographics and reasons
	@GetMapping(value = "/discontinued")
	@ResponseBody
	public Mapper getDiscontinued(){
		logger.info("Getting discontinued card list...");
		return cardService.getDiscontinued();
	}
}

