package com.five.bank.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.five.bank.BankApplication;
import com.five.bank.Mapper;
import com.five.bank.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.five.bank.domain.Customer;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path="/customers", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerController {

	static Logger logger = LogManager.getLogger(CustomerController.class);

	@Autowired
	CustomerService customerService;

	@ModelAttribute
	public void updateClassifications() {
		customerService.updateAllCustomerClassifications();
	}

	@GetMapping()
	@ResponseBody
	public List<Customer> getAllCustomers(){
		logger.info("Get Customers");
		List<Customer> list = customerService.customerList();
		list.forEach(a -> logger.debug(a));
		return list;
	}

	@PostMapping()
	@ResponseBody
	public String postCustomer(HttpServletRequest request){
		logger.info("Post Customer");
		logger.debug(request.getParameterMap());

		Map<String, String> customerData = BankApplication.paramaterMaptoMap(request.getParameterMap());

		return customerService.save(customerData, false);
	}

	@PutMapping()
	@ResponseBody
	public String editCustomer(HttpServletRequest request){
		logger.info("Put Customer");
		logger.debug(request.getParameterMap());

		Map<String, String> customerData = BankApplication.paramaterMaptoMap(request.getParameterMap());

		return customerService.save(customerData, true);
	}

	@DeleteMapping()
	@ResponseBody
	public String deleteCustomer(HttpServletRequest request) {
		logger.info("Delete Customer");
		logger.debug((request.getParameterMap()));

		Map<String, String> customerData = BankApplication.paramaterMaptoMap(request.getParameterMap());

		return customerService.deleteCustomer(customerData);
	}

	//Vi
	//7. get all spending history for specified customer
	//get spending history for one category (optional parameter)
	@GetMapping("/{id}/history/spending")
	@ResponseBody
	public Map<String, Map<Object, Object>> getAllCustomerTransactions(@PathVariable("id") int id, @RequestParam(name="category",required=false) Integer categoryId){
		logger.info("Get Customers");
		Map<String, Map<Object, Object>> transactions = customerService.getSpendHistory(id,categoryId);
		return transactions;
	}

	//Vi
	//11. get classification of customers
	@GetMapping("/classification")
	@ResponseBody
	public Map<String, Map<Object, Object>> getCustomerClassificationByPayment(){
		logger.info("Get Customer Classifications");
		Map<String, Map<Object, Object>> transactions = customerService.getClassificationByPayment();
		return transactions;
	}
	
	//Vi
	//15. get demographics of customers (Without customer info)
	@GetMapping("/demographics")
	@ResponseBody
	public Map<String, Map<String, Integer>> getCustomerDemographics(){
		logger.info("Get Customer Demographics");
		//K demographic, V how many customers belong in that demographic
		Map<String, Map<String, Integer>> demo = customerService.getAllCustomerDemographics();
		return demo;
	}	
	
	//Vi
	//15. get demographics of customers (With customer info)
	@GetMapping("/demographics/all")
	@ResponseBody
	public Map<String, Map<String, List<Object>>> getAllCustomerDemographics(){
		logger.info("Get Customer Demographics");
		//K demographic, V how many customers belong in that demographic
		Map<String, Map<String, List<Object>>> demo = customerService.getAllCustomerDemographicsList();
		return demo;
	}	
	//Vi
	//8. get all credit cards for specified customer with limits
	@GetMapping("/{id}/cards")
	@ResponseBody
	public Map<String, Map<Object, Object>> getAllCustomerCards(@PathVariable("id") int id){
		logger.info("Get Customer cards with limit");
		Map<String, Map<Object, Object>> transactions = customerService.getCardsWithLimit(id);
		return transactions;
	}	
	//Vi
	//7.get all credit cards transactions by category
	@GetMapping("/{id}/cards/usage")
	@ResponseBody
	public Map<String, List<Map<String, Object>>> getCreditTypeTransactions(@PathVariable("id") int id){
		logger.info("Get Customer credit card transactions");
		Map<String, List<Map<String, Object>>> transactions = customerService.getCreditCardUsage(id);
		return transactions;
	}
	
	// Zaid
	// MVP 9, get Credit card payment history along with the limits 
	@GetMapping("/{id}/cards/payment")
	@ResponseBody
	public Map<String, Mapper> getAllCustomerPayments(@PathVariable("id") int id){
		logger.info("Get Customers");

		return customerService.getPaymentHistory(id);
	}
}
