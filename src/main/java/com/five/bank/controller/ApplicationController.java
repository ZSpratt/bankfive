package com.five.bank.controller;

import com.five.bank.BankApplication;
import com.five.bank.domain.Application;
import com.five.bank.service.ApplicationService;

import com.five.bank.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/applications", produces = MediaType.APPLICATION_JSON_VALUE)
public class ApplicationController { 
	static Logger logger = LogManager.getLogger(ApplicationController.class);
	@Autowired
	ApplicationService service;

	@Autowired
	CustomerService customerService;

	@ModelAttribute
	public void updateClassifications() {
		customerService.updateAllCustomerClassifications();
	}

	@GetMapping
	@ResponseBody
	public List<Application> applicationList() {
		logger.info("Getting all applications...");
		List<Application> list = service.findAll();
		return list;
	}

	@PostMapping
	@ResponseBody
	public String addApplication(HttpServletRequest request) {
		logger.info("Inserting application...");

		return service.save(BankApplication.paramaterMaptoMap(request.getParameterMap()));

	}

	@PutMapping
	public String editApplication(HttpServletRequest request) {
		logger.info("Updating application...");

		return service.update(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

	@DeleteMapping
	@ResponseBody
	public String deleteApplication(HttpServletRequest request) {
		logger.info("Delete Application");

		return service.deleteApplication(BankApplication.paramaterMaptoMap(request.getParameterMap()));
	}

	@GetMapping(value ="/{id}")
	@ResponseBody
	public String getApplicationById(@PathVariable("id") int id) {
		logger.info("Getting application by id...");

		return service.findById(id);
	}
	
	//get by status, region/or profession
	@GetMapping(value ="/filter")
	public List<Application> getByStatusAndRegionOrProfession(HttpServletRequest request) {
		logger.info("Filtering applications by status, region, /or profession...");
		List<Application> list = service.filter(BankApplication.paramaterMaptoMap(request.getParameterMap()));

		return list;
	}	

	//report of applications received daily, weekly, or monthly
	@GetMapping(value ="/byDate")
	public List<Application> getByDateRange(@RequestParam(name="start") String start, @RequestParam(name="end") String end) {
		logger.info("Getting all applications by date...");
		List<Application> list = service.findByDateRange(start,end);
		return list;
	}
	
	@GetMapping(value ="/report")
	public Integer getTotalByDateRange(@RequestParam(name="start") String start, @RequestParam(name="end") String end) {
		logger.info("Getting total applications by date...");
		int total = service.findTotalByDateRange(start,end);
		return total;
	}	
	//Vi
	//10. Average time to close an application
	@GetMapping(value ="/averageTime")
	public String getAverageTime() {
		logger.info("Getting average application time");
		String time = service.findAverageTime();
		return time;
	}	
		

}
