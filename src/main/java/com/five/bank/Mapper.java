package com.five.bank;

import java.util.*;

public class Mapper extends ArrayList<Map<String, Object>> {

	public static Mapper convertToMapList(List<Object[]> data, String ...labels) {

		try {
			return ObjectArrayListToMapList(data, labels);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	//converts a list of object arrays to a list of hashmaps, allowing the data to be annotated appropriately
	private static Mapper ObjectArrayListToMapList(List<Object[]> data, String ...labels) throws Exception {
		Mapper output = new Mapper();

		if (data == null || data.size() == 0) {
			return null;
		}

		if (labels.length != data.get(0).length) {
			throw new Exception("Annotation count does not match object count");
		}

		for (Object[] result : data) {
			Map<String, Object> row = new LinkedHashMap<>();

			for (int i = 0; i < result.length; i++) {
				row.put(labels[i], result[i]);
			}

			output.add(row);
		}

		return output;
	}
}
