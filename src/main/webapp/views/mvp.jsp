<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

These are all of the MVP calls.
<br/>
<div>
  <div style="border:2px solid black;" id = "MVP2">
    MVP 2 : Number of Card/Loan received:<br/>
    <form id = "mvp2Form">

      <table>
          <tr>
              <th>Filter</th>
              <th>
                <select id="filter" name="filter">
                  <option value="Day">Day</option>
                  <option value="Week">Week</option>
                  <option value="Month">Month</option>
                  <option value="Year">Year</option>
                </select>
              </th>
          </tr>
      </table>
    </form>
    <button onclick="getRequest(mvp2Form, '/loans/applications/date', mvp2Result)">Filter Loans</button>
    <button onclick="getRequest(mvp2Form, '/cards/applications/date', mvp2Result)">Filter Cards</button>
    <button onclick="mvp2Result.innerHTML=''">Clear</button>
    <br/>
    <br/>
    <div id="mvp2Result">

    </div>
  </div>

  <div style="border:2px solid black;" id = "MVP3">
    MVP 3 : Status of Application:<br/>

    <form id = "mvp3Form">

      <table>
          <tr>
              <th>Prospect</th>
              <th>
                <select id="prospect" name="prospect">
                  <option value=""></option>
                  <option value="True">True</option>
                  <option value="False">False</option>
                </select>
              </th>
          </tr>
          <tr>
              <th>Status</th>
              <th>
                <select id="status" name="status">
                  <option value=""></option>
                  <option value="Submitted">Submitted</option>
                  <option value="In Progress">In Progress</option>
                  <option value="Accepted">Accepted</option>
                  <option value="Denied">Denied</option>
                </select>
              </th>
          </tr>
          <tr>
              <th>Profession</th>
              <th>
                <select id="profession" name="profession">
                  <option value=""></option>
                  <option value="N/A">N/A</option>
                  <option value="Teacher">Teacher</option>
                  <option value="Lawyer">Lawyer</option>
                  <option value="Plumber">Plumber</option>
                  <option value="Farmer">Farmer</option>
                  <option value="Dentist">Dentist</option>
                  <option value="Software Engineer">Software Engineer</option>
                  <option value="Streamer">Streamer</option>
                  <option value="Independent Contractor">Independent Contractor</option>
                </select>
              </th>
          </tr>
          <tr>
            <th>City</th>
            <th><input id="city" name="city" />
          </tr>
          <tr>
            <th>State</th>
            <th><input id="state" name="state" /></th>
          </tr>
      </table>
    </form>
    <button onclick="getRequest(mvp3Form, '/applications/filter', cardDelResult)">Filter Applications</button>
    <button onclick="cardDelResult.innerHTML=''">Clear</button>
    <br/>
    <br/>
    <div id="cardDelResult">

    </div>
  </div>

  <div style="border:2px solid black;" id = "MVP4">
    MVP 4 : Cards approved by Region/Profession:<br/>
    <form id = "mvp4Form">

      <table>
          <tr>
              <th>Filter</th>
              <th>
                <select id="filter" name="filter">
                  <option value="Region">Region</option>
                  <option value="Profession">Profession</option>
                </select>
              </th>
          </tr>
      </table>
    </form>
    <button onclick="getRequest(mvp4Form, '/cards/approved', mvp4Result)">Filter Loans</button>
    <button onclick="mvp4Result.innerHTML=''">Clear</button>
    <br/>
    <br/>
    <div id="mvp4Result">

    </div>
  </div>

  <div style="border:2px solid black;" id = "MVP5">
    MVP 5 : Cards Rejected with Reasons<br/>

    <button onclick="getRequest(null, '/cards/denied', mvp5Result)">Reason Counts</button>
    <button onclick="mvp5Result.innerHTML=''">Clear</button>
    <br/>
    <br/>
    <div id="mvp5Result">

    </div>
  </div>

  <div style="border:2px solid black;" id = "MVP6">
    MVP 6 : Card Statement:<br/>
    <form id = "mvp6Form">

      <table>
          <tr>
              <th>Card Number</th>
              <th>
                <input id="card_number" name="card_number" />
              </th>
          </tr>
      </table>
    </form>
    <button onclick="getRequest(null, '/cards/' + ((card_number.value.trim() == '') ? 0 : card_number.value) + '/statement', mvp6Result)">Filter Loans</button>
    <button onclick="mvp6Result.innerHTML=''">Clear</button>
    <br/>
    <br/>
    <div id="mvp6Result">

    </div>
  </div>

  <div style="border:2px solid black;" id = "MVP7">
    MVP 7: Customer Card Usage/ Customer Spending History<br>
    <form id="mvp7Form">
    <label for="customer_id">Customer ID</label>
    <input id="customer_id" name="customer_id" type=number step="1">
    </form>
    <button onclick="getRequestToTable('cardUsage',mvp7Form,'/customers', '/cards/usage', mvp7Result)">Card Usage</button>
    <button onclick="getRequestToTable('spendHistory',mvp7Form,'/customers', '/history/spending', mvp7Result)">Spend History</button>
    <button onclick="mvp7Result.innerHTML = ''">Clear</button>
    <br/>
    <div id="mvp7Result">

    </div>
    </div>

  <div style="border:2px solid black;" id = "MVP8">
  MVP 8: Customer Credit Card Limits<br>
    <form id="mvp8Form">
        <label for="customer_id">Customer ID</label>
    <input id="customer_id" name="customer_id" type=number step="1">
    </form>

   <button onclick="getRequestToTable('cardLimits',mvp8Form,'/customers', '/cards',mvp8Result)">Card Limits</button>
   <button onclick="mvp8Result.innerHTML = ''">Clear</button>
   <br/>
   <div id="mvp8Result">

    </div>
    </div>

    <div style="border:2px solid black;" id = "MVP9">
    MVP 9 : Credit card payment history along with the limits <br/>

    <form id = "mvp9Form">

      <table>
          <tr>
              <th>Customer ID</th>
              <th>
                <input id="id9" name="id9" type="number" />
              </th>
          </tr>
      </table>
    </form>

    <button onclick="getRequestPaymentHistory(null, '/customers/' + ((id9.value.trim() == '') ? 0 : id9.value) + '/cards/payment', mvp9Result)">Get Cards</button>
    <button onclick="mvp9Result.innerHTML=''">Clear</button>
    <br/>
    <br/>
    <div id="mvp9Result">

    </div>
  </div>

      <div style="border:2px solid black;" id = "MVP10">
        MVP 10: Average Time to Process an Application<br>
    <button onclick="getRequestToTable('averageTime',null,'/applications/averageTime',null, mvp10Result)">Average Application Time</button>
    <br/>
    <div id="mvp10Result">

    </div>
    </div>

      <div style="border:2px solid black;" id = "MVP11">
      MVP 11: Customer Classification<br>
    <button onclick="getRequestToTable('classification',null,'/customers/classification',null, mvp11Result)">Classification</button>
    <br/>
    <div id="mvp11Result">

    </div>
    </div>

    <div style="border:2px solid black;" id = "MVP12">
    MVP 12 : Credit card expiring in next 3 months  <br/>

    <button onclick="getRequest(null, '/cards/expired', mvp12Result)">Get Cards</button>
    <button onclick="mvp12Result.innerHTML=''">Clear</button>
    <br/>
    <br/>
    <div id="mvp12Result">

    </div>
  </div>

    <div style="border:2px solid black;" id = "MVP13">
    MVP 13 : Region wise usage of Credit card<br/>
    <form id = "mvp13Form">

      <table>
          <tr>
              <th>Card Number</th>
              <th>
                <input id="card_number13" name="card_number" />
              </th>
          </tr>
      </table>
    </form>
    <button onclick="getRequest(null, '/cards/' + ((card_number13.value.trim() == '') ? 0 : card_number13.value) + '/regions', mvp13Result)">Submit</button>
    <button onclick="mvp13Result.innerHTML=''">Clear</button>
    <br/>
    <br/>
    <div id="mvp13Result">

    </div>
  </div>

    <div style="border:2px solid black;" id = "MVP15">
    MVP 15: Customer Demographics<br>
     <button onclick="getDemographics('/customers/demographics/all', mvp15Result)">Customer Demographics</button>
   <br/>
    <div id="mvp15Result">

    </div>
    </div>

    <div style="border:2px solid black;" id = "MVP16">
    MVP 16 : No. of credit card discontinued customer wise with region and demographics and reasons<br/>

    <button onclick="getRequest(null, '/cards/discontinued', mvp16Result)">Get Cards</button>
    <button onclick="mvp16Result.innerHTML=''">Clear</button>
    <br/>
    <br/>
    <div id="mvp16Result">

    </div>
  </div>

</div>
