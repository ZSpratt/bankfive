<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


On this page, we can send forms to the server and get/post/put Card data, the result of which will appear under the form.
<br/>
<div>
  <div style="border:2px solid black;" id = "cardGet">
    Get Loan:<br/>
    <button onclick="getRequest(null, '/loans', loansGetResult)">Get Loan</button>
    <button onclick="loansGetResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="loansGetResult">

    </div>
  </div>

  <div style="border:2px solid black;" id = "loansPut/>Post">
    Post/Put Loan:<br/>
    Customer ID is pulled from logged in customer
    <form id = "loansPostForm">
      <table>
           <input id="customer_id" name="customer_id" value="${Customer.id}"/></th>
          <tr>
              <th>Balance</th>
              <th><input id="balance" name="balance" type="number"/></th>
          </tr>
          <tr>
              <th>City</th>
              <th><input id="city" name="city"/></th>
          </tr>
          <tr>
              <th>State (2 letters)</th>
              <th><input id="state" name="state"/></th>
          </tr>
          <tr><th colspan="2">Used for PUT</th></tr>
          <tr>
            <th>Loan ID</th>
            <th><input id="id" name="id"/></th>
         </tr>
      </table>
    </form>
    <button onclick="postRequest(loansPostForm, '/loans', loansPostResult)">Post Loan</button>
    <button onclick="putRequest(loansPostForm, '/loans', loansPostResult)">Put Loan</button>
    <button onclick="loansPostResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="loansPostResult">

    </div>
  </div>

  <div style="border:2px solid black;" id = "loanDelete">
    Delete Loan:<br/>
    <form id = "loanDelForm">
      <table>
          <tr>
              <th>Loan ID</th>
              <th><input id="id" name="id"/></th>
          </tr>
      </table>
    </form>
    <button onclick="deleteRequest(loanDelForm, '/loans', loanDelResult)">Delete Loan</button>
    <button onclick="loanDelResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="loanDelResult">

    </div>
  </div>
</div>
