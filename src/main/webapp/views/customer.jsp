<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

On this page, we can send forms to the server and get/post/put Customer data, the result of which will appear under the form.
<br/>
<div>
  <div style="border:2px solid black;" id = "customerGet">
    Get Customer:<br/>
    <button onclick="getRequest(null, '/customers', customersGetResult)">Get Customer</button>
    <button onclick="customersGetResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="customersGetResult">

    </div>
  </div>

  <div style="border:2px solid black;" id = "customerPut/>Post">
    Post/Put Customer:<br/>
    <form id = "customerPostForm">
      <table>
          <tr><th colspan="2">Used for PUT and POST</th></tr>
          <tr>
              <th>Username</th>
              <th><input id="username" name="username"/></th>
          </tr>
          <tr>
              <th>Password</th>
              <th><input id="password" name="password" type="password"/></th>
          </tr>
          <tr>
              <th>Prospect</th>
              <th>
                <select id="prospect" name="prospect" type="checkbox" checked="true">
                  <option value=""></option>
                  <option value="True">Prospect Customer</option>
                  <option value="False">Active Customer</option>
                </select>
              </th>
          </tr>
          <tr>
              <th>First Name</th>
              <th> <input id="firstname" name="firstname"/></th>
          </tr>
          <tr>
              <th>Last Name</th>
              <th><input id="lastname" name="lastname"/></th>
          </tr>
          <tr>
              <th>E-Mail</th>
              <th><input id="email" name="email"/></th>
          </tr>
          <tr>
              <th>Pin</th>
              <th><input type="number" id="pin" name="pin"/></th>
          </tr>
          <tr>
              <th>Phone Number</th>
              <th><input id="phonenumber" name="phonenumber"/></th>
          </tr>
          <tr>
              <th>Address</th>
              <th><input id="address" name="address"/></th>
          </tr>
          <tr>
              <th>City</th>
              <th><input id="city" name="city"/></th>
          </tr>
          <tr>
              <th>State</th>
              <th><input id="state" name="state"/></th>
          </tr>
          <tr>
              <th>Zip</th>
              <th><input id="zip" name="zip"/></th>
          </tr>
          <tr>
              <th>Gender</th>
              <th>
                <select id="gender" name="gender">
                  <option value=""></option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
              </th>
          </tr>
          <tr>
              <th>Age</th>
              <th><input type="number" id="age" name="age"/></th>
          </tr>
          <tr>
              <th>Profession</th>
              <th>
                <select id="profession_id" name="profession_id"/>
                  <option value=""></option>
                  <option value="0">N/A</option>
                  <option value="1">Teacher</option>
                  <option value="2">Lawyer</option>
                  <option value="3">Plumber</option>
                  <option value="4">Farmer</option>
                  <option value="5">Dentist</option>
                  <option value="6">Software Engineer</option>
                  <option value="7">Streamer</option>
                  <option value="8">Independent Contractor</option>
                </select>
              </th>
          </tr>
      </table>
    </form>
    <button onclick="postRequest(customerPostForm, '/customers', customersPostResult)">Post Customer</button>
    <button onclick="putRequest(customerPostForm, '/customers', customersPostResult)">Put Customer</button>
    <button onclick="customersPostResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="customersPostResult">

    </div>
  </div>

  <div style="border:2px solid black;" id = "customerDelete">
    Delete Customer:<br/>
    <form id = "customerDelForm">
      <table>
          <tr>
              <th>Username</th>
              <th><input id="username" name="username"/></th>
          </tr>
      </table>
    </form>
    <button onclick="deleteRequest(customerDelForm, '/customers', customersDelResult)">Delete Customer</button>
    <button onclick="customersDelResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="customersDelResult">

    </div>
  </div>
</div>
