<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

On this page, we can send forms to the server and get/post/put Card data, the result of which will appear under the form.
<br/>
<div>
  <div style="border:2px solid black;" id = "cardGet">
    Get Card:<br/>
    <button onclick="getRequest(null, '/cards', cardsGetResult)">Get Card</button>
    <button onclick="cardsGetResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="cardsGetResult">

    </div>
  </div>

  <div style="border:2px solid black;" id = "cardPut/>Post">
    Post/Put Card:<br/>
    <form id = "cardPostForm">
      <table>
          <tr>
              <th>Card Number</th>
              <th><input id="card_number" name="card_number"/></th>
          </tr>
          <tr><th colspan="2">Used for POST</th></tr>
          <tr>
            <th>Customer Id (logged in user)</th>
            <th><input id="customer_id" name="customer_id" value="${Customer.id}"/></th>
          </tr>
          <tr>
              <th>Card Type</th>
              <th>
                <select id="type" name="type">
                  <option value=""></option>
                  <option value="0">Visa Credit</option>
                  <option value="1">Discover Credit</option>
                  <option value="2">American Express Credit</option>
                  <option value="3">Mastercard Credit</option>
                </select>
              </th>
          </tr>
          <tr>
              <th>Security Code</th>
              <th><input id="security_code" name="security_code" type="number"/></th>
          </tr>
          <tr>
              <th>Card Balance</th>
              <th><input id="card_balance" name="card_balance" type="number"/></th>
          </tr>
          <tr>
              <th>Card Limit</th>
              <th><input id="card_limit" name="card_limit" type="number"/></th>
          </tr>
          <tr><th colspan="2">Used for PUT</th></tr>
          <tr>
              <th>Discontinue Reason</th>
              <th>
                <select id="discontinue_reason" name="discontinue_reason">
                  <option value=""></option>
                  <option value="0">Open</option>
                  <option value="1">Expired</option>
                  <option value="2">Canceled</option>
                  <option value="3">Lost</option>
                  <option value="4">Stolen</option>
                </select>
              </th>
          </tr>
      </table>
    </form>
    <button onclick="postRequest(cardPostForm, '/cards', cardPostResult)">Post Card</button>
    <button onclick="putRequest(cardPostForm, '/cards', cardPostResult)">Put Card</button>
    <button onclick="cardPostResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="cardPostResult">

    </div>
  </div>

  <div style="border:2px solid black;" id = "cardDelete">
    Delete Card:<br/>
    <form id = "cardDelForm">
      <table>
          <tr>
              <th>Card Number</th>
              <th><input id="card_number" name="card_number"/></th>
          </tr>
      </table>
    </form>
    <button onclick="deleteRequest(cardDelForm, '/cards', cardDelResult)">Delete Card</button>
    <button onclick="cardDelResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="cardDelResult">

    </div>
  </div>
</div>
