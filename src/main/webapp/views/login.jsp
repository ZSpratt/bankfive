<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<div style="text-align:left; justify-content: left; align-items: center;">
<span class="headerLogo">Bank 504</span>
<c:if test="${not empty Customer}">
    <div id="userData">
        ${Customer.firstName} ${Customer.lastName}<br>
        <button onclick="logout()">Log Out</button>

        <script>
            function logout() {
                formData = new FormData();

                request = new XMLHttpRequest()
                request.open("POST", "/logout")
                request.send(formData);

                request.onreadystatechange=(e) => {
                    console.log(request.responseText)
                    if (request.responseText.includes("LoginSuccessful")) {
                        location.reload()
                    }
                }
            }
        </script>
    </div>
</c:if>
<c:if test="${empty Customer}">
        <div id="loginDiv">
        <form id="loginForm">
          <table>
              <tr>
                  <th>Username</th>
                  <th><input id="username" name="username"></th>
              </tr>
              <tr>
                  <th>Password</th>
                  <th><input type="password" id="password" name="password"></th>
              </tr>
          </table>
          <div id="loginError"></div>
        </form>
        <button onclick="login(username.value, password.value)">Submit</button>

        <script>
            function login(user, pass) {
                formData = new FormData(loginForm);

                request = new XMLHttpRequest()
                request.open("POST", "/login")
                request.send(formData);

                request.onreadystatechange=(e) => {
                    console.log(request.responseText)
                    if (request.responseText.includes("LoginSuccessful")) {
                        location.reload()
                    } else {
                        loginError.innerHTML = "Login Failed"
                    }
                }
            }
        </script>
    </div>
</c:if>
</div>
