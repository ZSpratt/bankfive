<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
  <head>
    <meta charset="ISO-8859-1">
    <title>Bank 504</title>

    <style><%@include file="style.css"%></style>
  </head>
  <body>
    <script>
      <%@include file="index.js"%>
    </script>
    <div id="header">
      <div id="navBar" class="navBar">
        <div class="navBar_button button">
          <jsp:include page="login.jsp" flush="true"/>
        </div>
        <a class="navBar_button button" href="/">Home</>
        <a class="navBar_button button" href="/customer">Customer</a>
        <a class="navBar_button button" href="/card">Card</a>
        <a class="navBar_button button" href="/application">Application</a>
        <a class="navBar_button button" href="/loan">Loan</a>
        <a class="navBar_button button" href="/transaction">Transaction</a>
        <a class="navBar_button button" href="/mvp">MVP Methods</a>
      </div>
    </div>
    <div id="content">
      <c:if test="${not empty TBody}">
          <jsp:include page="${TBody}.jsp" flush="true"/>
      </c:if>
      <c:if test="${empty TBody}">
          Page failed or missing
      </c:if>
    </div>
  </body>
</html>
