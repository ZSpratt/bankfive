<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
On this page, we can send forms to the server and get/post/put Transaction data, the result of which will appear under the form.
<div>
  <div style="border:2px solid black;" id = "transactionGet">
    Get Transactions:<br/>
    <button onclick="getRequest(null, '/transactions', transactionsGetResult)">Get Transactions</button>
    <button onclick="transactionsGetResult.innerHTML=''">Clear Results</button>
    <br>
    <br>
    <div id="transactionsGetResult"></div>
  </div>

  <div style="border:2px solid black;" id = "transactionPut/Post">
      Post/Put Transaction:<br/>

      <form id = "transactionPostForm">
        <!-- required. amount,state,merchant_name, category_id, customer_id -->
        <table>
          <tr>
            <th><label for="customer_id">Customer ID (logged in user)</label></th>
            <th><input id="customer_id" name="customer_id" step="1" type="number" value="${Customer.id}"></th>
          </tr>
          <tr>
            <th><label for="amount">Amount</label></th>
            <th><input id="amount" name="amount" step="0.01" type="number"></th>
          </tr>
          <tr>
            <th><label for="merchant_name">Merchant Name</label></th>
            <th><input id="merchant_name" name="merchant_name" type="text"></th>
          </tr>
          <tr>
            <th><label for="category_id">Category</label></th>
            <th>
              <select id="category_id" name="category_id">
                  <option value=""></option>
                  <option value="0">Credit Payment</option>
                  <option value="1">Other</option>
                  <option value="2">Groceries</option>
                  <option value="3">Entertainment</option>
                  <option value="4">Restaurant & Dining</option>
                  <option value="5">Utilities</option>
                  <option value="6">Transportation</option>
                  <option value="7">Personal</option>
                  <option value="8">Insurance</option>
                  <option value="9">Health</option>
                  <option value="10">Travel</option>
                  <option value="11">Miscellaneous</option>
                </select>
              </th>
          </tr>
          <tr>
            <th><label for="card_number">Card Number</label></th>
            <th><input id="card_number" name="card_number" type="text"></th>
          </tr>
          <tr>
            <th><label for="loan_id">Loan ID</label></th>
            <th><input id="loan_id" name="loan_id" step="1" type="number"></th>
          </tr>
          <tr>
            <th><label for="description">Description</label></th>
            <th><input id="description" name="description" type="text"></th>
          </tr>
          <tr><th colspan="2">Used for PUT</th></tr>
          <tr>
            <th><label for="id">Transaction ID</label></th>
            <th><input id="id" name="id" step="1" type="number"></th>
          </tr>
        </table>
      </form>

      <button onclick="postRequest(transactionPostForm, '/transactions', transactionsPutResult)">Post Transaction</button>
      <button onclick="putRequest(transactionPostForm, '/transactions', transactionsPutResult)">Put Transaction</button>
      <button onclick="transactionsPutResult.innerHTML=''">Clear Results</button>
      <br>
      <br>
      <div id="transactionsPutResult"></div>
    </div>

    <div style="border:2px solid black;" id = "transactionPut/Post">
      Delete Transaction:<br/>
      <form id = "transactionDeleteForm">
        <table>
        <tbody>
        <tr> <td>
        <label for="id">Transaction ID</label>
        <input id="id" name="id" step="1" type="number">
        </td></tr>
        </tbody>
        </table>
      </form>
      <button onclick="deleteRequest(transactionDeleteForm, '/transactions', transactionsDelResult)">Delete Transaction</button>
      <button onclick="transactionsDelResult.innerHTML=''">Clear Results</button>
      <br>
      <br>
      <div id="transactionsDelResult"></div>
    </div>

  </div>
