function IsJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

function arrayToHTMLTable(dataObject) {
  //getHeader
  jsonTable = document.createElement("table")
  jsonTable.classList.add("results")

  columns = Object.keys(dataObject[0])
  //console.log(columns)

  row = jsonTable.insertRow()
  row.classList.add("results")
  row.classList.add("headerRow")
  for (column in columns) {
    if (columns[column] != "password") {
      cell = row.insertCell(-1)
      cell.innerHTML = columns[column]
      cell.classList.add("results")
    }
  }

  for (i in dataObject){
    rowObject = dataObject[i]
    row = jsonTable.insertRow()
    row.classList.add("results")

    columns = Object.keys(dataObject[i])
    //console.log(columns)

    row = jsonTable.insertRow()
    for (column in columns) {
      if (columns[column] != "password") {
        cell = row.insertCell(-1)
        cell.classList.add("results")
        cellObject = dataObject[i][columns[column]]
        //console.log(cellObject)
        console.log(columns[column])
        if (cellObject == null || cellObject == "null") {
          cell.innerHTML = ""
        } else if ("id" === Object.keys(cellObject)[0]) {
          if (Object.keys(cellObject)[1] == "dateOpened") {
            cell.innerHTML = cellObject[Object.keys(cellObject)[0]]
          } else {
            cell.innerHTML = cellObject[Object.keys(cellObject)[1]]
          }
        } else {
          if (columns[column].toLowerCase().includes("date") || columns[column].toLowerCase().includes("week")) {
            cell.innerHTML = cellObject.split("T")[0]
          } else {
            cell.innerHTML = cellObject
          }
        }
      }
    }
  }

  return jsonTable
}

function jsonToHTML(data) {
  if (!IsJsonString(data)) {
    console.log(data)
    return "Not JSON Data"
  }
  dataObject = JSON.parse(data)
  //return JSON.stringify(dataObject, null, "  ");
  passfail = ""
  if (!Array.isArray(dataObject)) {
    if ("Success" === Object.keys(dataObject)[0]) {
      passfail = "Success"
      sub = dataObject["Success"]
      if (typeof sub === 'string') {
        return "Success : " + sub
      }
      dataObject = [sub[Object.keys(sub)[0]]]
      console.log(dataObject)
    } else if ("Failed" === Object.keys(dataObject)[0]) {
      passfail = "Failed"
      return "Failed : " + dataObject["Failed"]
    }
  }
  if (Array.isArray(dataObject)) {

    if (dataObject.length == 0){
      return "0 results"
    }

    jsonTable = arrayToHTMLTable(dataObject)

    //console.log(jsonTable.outerHTML)
    return passfail + jsonTable.outerHTML
  } else {
    return JSON.stringify(dataObject, null, "  ");
  }
}

function cleanFormData(formData) {
  keys = formData.keys()

  deleteKeys = []
  for (const key of keys) {
    if (formData.get(key).trim() === "") {
      deleteKeys.push(key)
    }
  }
  for (key of deleteKeys) {
    formData.delete(key)
  }
  keys = formData.keys()

  return formData;
}

function getRequest(formSource, requestAddress, outputDiv) {
  if (formSource != null) {
    requestAddress += "?"
    formData = cleanFormData(new FormData(formSource));
    keys = formData.keys()
    for (key of keys) {
      requestAddress += key + "=" + formData.get(key) + "&"
    }
  }

  request = new XMLHttpRequest()
  request.open("GET", requestAddress)
  request.send();

  request.onreadystatechange=(e) => {
    outputDiv.innerHTML = jsonToHTML(request.responseText);
  }
}

function postRequest(formSource, requestAddress, outputDiv) {
  request = new XMLHttpRequest()
  request.open("POST", requestAddress)
  if (formSource != null) {
    formData = cleanFormData(new FormData(formSource));
    request.send(formData);
  } else {
    request.send()
  }

  request.onreadystatechange=(e) => {
    outputDiv.innerHTML = jsonToHTML(request.responseText);
  }
}

function putRequest(formSource, requestAddress, outputDiv) {
  request = new XMLHttpRequest()
  request.open("PUT", requestAddress)

  if (formSource != null) {
    formData = cleanFormData(new FormData(formSource));
    request.send(formData);
  } else {
    request.send()
  }

  request.onreadystatechange=(e) => {
    outputDiv.innerHTML = jsonToHTML(request.responseText);
  }
}

function deleteRequest(formSource, requestAddress, outputDiv) {
  request = new XMLHttpRequest()
  request.open("DELETE", requestAddress)

  if (formSource != null) {
    formData = cleanFormData(new FormData(formSource));
    request.send(formData);
  } else {
    request.send()
  }

  request.onreadystatechange=(e) => {
    outputDiv.innerHTML = jsonToHTML(request.responseText);
  }
}
//make a Map<String, Map<Object, Object>> to a table
function getRequestToTable(functionality, formSource,baseAddress, endAddress, outputDiv) {
   request = new XMLHttpRequest()

  customerId = formSource==null ? null : formSource.elements[0].value;

  requestAddress = endAddress==null ?  baseAddress : baseAddress+ "/" + customerId + endAddress;
  request.open("GET", requestAddress)

   if(customerId=="" && endAddress != null)
  {
	outputDiv.innerHTML =  "Customer Id is not provided";
	return;
   }

  if (formSource != null) {
    formData = cleanFormData(new FormData(formSource));
    request.send(formData);
  } else {
    request.send()
  }

console.log(requestAddress)

  request.onreadystatechange= () => {
	data = request.responseText;
	parsedData = !IsJsonString(data) ? data : JSON.parse(data);

	table = document.createElement("table");
    table.classList.add("results");
	tbody = document.createElement("tbody");

	columns =[];

	switch(functionality)
	{
		case "cardUsage": //MVP 7
		columns = ["Credit Card (last 4-digits)", "Transaction Amount ($)", "Category", "Merchant Name", "Transaction Date","Transaction ID"];
		table = getCardUsage(columns,6, parsedData);
		break;
		case "spendHistory": //MVP 7
		columns =["Category", "# of Transactions", "Total Spent ($)"];
		table = getInfoPerCustomer(columns,3,parsedData);
		break;
		case "cardLimits": //MVP 8
		columns =["","Credit Card (last 4-digits)", "Card Limit ($)","Card Balance ($)"];
		table = getInfoPerCustomer(columns,4,parsedData);
		break;
		case "averageTime": //MVP 10
		row = document.createElement("tr");
		cell = document.createElement("td");
		cell.classList.add("tableResults");
		
		cellText = document.createTextNode(parsedData);

		cell.appendChild(cellText);
		row.appendChild(cell);

		tbody.appendChild(row);
		table.appendChild(tbody);
		break;
		case "classification": //MVP 11
		columns =["Customer","ID", "Name", "Classification"];
		table = getInfoPerCustomer(columns,4,parsedData);
		break;
	}

	console.log(table)
    outputDiv.innerHTML = table.outerHTML;
  }
}

function getInfoPerCustomer(columns,colSpan,parsedData) {
			//add type to individual row
		hrow = document.createElement("tr");
		//add columns to row
		//console.log(columns);
		for(x=0; x < columns.length; x++)
		{
			hcell = document.createElement("td");
			hcell.classList.add("tableResults");
			
		    hcellText = document.createTextNode(columns[x]);
            hcell.appendChild(hcellText);
            hrow.appendChild(hcell);
		}

		hrow.classList.add("headerRow");
		tbody.appendChild(hrow);

	for(type in parsedData)
	{
		//type = String key
		items = parsedData[type];

		row = document.createElement("tr");
		cell = document.createElement("td");
		cell.classList.add("tableResults");
		
		cellText = document.createTextNode(type);

		cell.appendChild(cellText);
		row.appendChild(cell);

		//console.log("type: " + type);
		for(i in items)
		{
			val = items[i];
			//console.log("i:"+ i + " item[i]: " + val);
			//append value to row
		    cell = document.createElement("td");
            cell.classList.add("tableResults");
		    cellText = document.createTextNode(val);
		    cell.appendChild(cellText);
			row.appendChild(cell);
		}
		tbody.appendChild(row);
	}
	table.appendChild(tbody);
	return table;
}

function getCardUsage(columns,colSpan,parsedData) {
			//add type to individual row
		hrow = document.createElement("tr");
		//add columns to row
		for(x=0; x < columns.length; x++)
		{
			hcell = document.createElement("td");
			hcell.classList.add("tableResults");
			
		    hcellText = document.createTextNode(columns[x]);
            hcell.appendChild(hcellText);
            hrow.appendChild(hcell);
		}

		hrow.classList.add("headerRow");
		tbody.appendChild(hrow);

	for(type in parsedData)
	{
		//type = String key
		items = parsedData[type];
		console.log(type);
		hrow = document.createElement("tr");
		hrow.classList.add("headerRow");
		hcell = document.createElement("td");
		hcell.classList.add("tableResults");
		
		hcell.colSpan = colSpan;
		hrow.appendChild(hcell);
		tbody.appendChild(hrow);

		console.log("type: " + type);
		for(i in items)
		{
			obj = items[i];
			//console.log("i:"+ i + " item[i]: " + obj);
				row = document.createElement("tr");
		cell = document.createElement("td");
		cell.classList.add("tableResults");
		
		cellText = document.createTextNode(type);
		       cell.appendChild(cellText);
		       row.appendChild(cell);
				for(j in obj)
			{
				//console.log("j: " + j + " val: "+obj[j]);
				//add value to row
				val = obj[j];
                cell = document.createElement("td");
                cell.classList.add("tableResults");
		        cellText = document.createTextNode(val);
		        cell.appendChild(cellText);
                //truncate date
                  if(j=="Transaction date")
                 {
                   cell.innerHTML = val.split("T")[0]
                 }
                row.appendChild(cell);   
			}
			//append row to body
                tbody.appendChild(row);
		}

	}
	table.appendChild(tbody);
	return table;
}

function getDemographics(requestAddress, outputDiv) {
  request = new XMLHttpRequest()
  request.open("GET", requestAddress)
  request.send()

  request.onreadystatechange= () => {
	data = request.responseText;
	parsedData = JSON.parse(data);

//table
table = document.createElement("table")
table.classList.add("results")
tbody = document.createElement("tbody");

columns = ["Category", " # of Customers ", "Customer(s)"];
		hrow = document.createElement("tr");
		//add columns to row
		for(x=0; x < columns.length; x++)
		{
			hcell = document.createElement("td");
			hcell.classList.add("tableResults");
		    
            hcellText = document.createTextNode(columns[x]);
            hcell.appendChild(hcellText);
            hrow.appendChild(hcell);
		}

		hrow.classList.add("headerRow");
		tbody.appendChild(hrow);


//main category: profession,age, etc.
    for(category in parsedData)
{
	rows =parsedData[category]
	console.log("rows: " + rows)
	console.log("category: " + category);

	row = document.createElement("tr");
	cell = document.createElement("td");
	cell.classList.add("tableResults");
	
    cellText = document.createTextNode(category);
	cell.appendChild(cellText);
    cell.colSpan=3;
	row.appendChild(cell);
	row.classList.add("subcategory");
    tbody.appendChild(row);

	//subcategory: farmer, 18-24,etc.
	for(subcategory in rows)
	{
		items = rows[subcategory];
		console.log("subcategory: " + subcategory )
		console.log("items: " + items);
		row = document.createElement("tr");
		cell = document.createElement("td");
		cell.classList.add("tableResults");
		
        cellText = document.createTextNode(subcategory);
        row.classList.add("results");
	    cell.appendChild(cellText);
        row.appendChild(cell);

        //no customers in that subcategory
        if(items==null)
        {
     	   cell = document.createElement("td");
           cellText = document.createTextNode("N/A");

           cell.classList.add("tableResults");
		   cell.appendChild(cellText);	
	       row.appendChild(cell);
     	   
           cell = document.createElement("td");	
           cell.classList.add("tableResults");       

           cellText = document.createTextNode("N/A");
		   cell.appendChild(cellText);
           row.appendChild(cell);
           tbody.appendChild(row);
        }else{
        		//row item: total customers or customer list
        //total customers for subcategory
        totalStr = items[0]
        total = totalStr[totalStr.length-1]

		cell = document.createElement("td");	
		cell.classList.add("tableResults");
		
		cellText = document.createTextNode(total);
		cell.appendChild(cellText);
	    row.appendChild(cell);

        cell = document.createElement("td");
        cell.classList.add("tableResults");
        customers = "";

        for(index in items)
		{
			item = items[index];
			if(index>0)
			{
				//add a "," if more than one customer in list
				customers = index==1 ? item : customers + ", " + item;
			}
		}
		//add customer info to row
		cellText = document.createTextNode(customers);
		cell.classList.add("cellOverflow");
		cell.appendChild(cellText);
	    row.appendChild(cell);
        tbody.appendChild(row);
       }
    }
   tbody.appendChild(row);
}
    table.appendChild(tbody);
    console.log(table);
    outputDiv.innerHTML =!IsJsonString(data)? "Not JSON Data" : table.outerHTML;
  }
}

function getRequestPaymentHistory(formSource, requestAddress, outputDiv) {
  if (formSource != null) {
    requestAddress += "?"
    formData = cleanFormData(new FormData(formSource));
    keys = formData.keys()
    for (key of keys) {
      requestAddress += key + "=" + formData.get(key) + "&"
    }
  }

  request = new XMLHttpRequest()
  request.open("GET", requestAddress)
  request.send();

  request.onreadystatechange=(e) => {
    data = request.responseText;
    outputDiv.innerHTML = "";
    if(IsJsonString(data)){
        var parsedData = JSON.parse(data);
        keys = Object.keys(parsedData);
        console.log(keys);

        if(keys.length == 0){
          outputDiv.innerHTML = "No reuslts";
        }

        for(key of keys){
          outputDiv.innerHTML += key + "<br>";
          outputDiv.innerHTML += arrayToHTMLTable(parsedData[key]).outerHTML+ "<br>";
        }
    }
  }
}
