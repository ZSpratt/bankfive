<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

On this page, we can send forms to the server and get/post/put Application data, the result of which will appear under the form.
<br/>
<div>
  <div style="border:2px solid black;" id = "applicationGet">
    Get Application:<br/>
    <button onclick="getRequest(null, '/applications', applicationsGetResult)">Get Applications</button>
    <button onclick="applicationsGetResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="applicationsGetResult"></div>
  </div>

  <div style="border:2px solid black;" id = "cardPut/>Post">
    Post/Put Card:<br/>
    <form id = "applicationPostForm">
      <table>
        <tr><th colspan="2">Used for POST</th></tr>
        <tr>
          <th>Username (logged in user)</th>
          <th><input id="username" name="username" value="${Customer.username}"/></th>
        </tr>
        <tr>
          <th>Application Type</th>
          <th>
            <select id="application_type" name="application_type" onchange="toggleLoanAmountDisplay()">
              <option value="">Select Loan Type</option>
              <option value="Loan">Loan</option>
              <option value="Credit">Credit</option>
            </select>
          </th>
        </tr>
        <tr id="LoanAmountToggle" style="display:none;">
          <th>Loan Amount</th>
          <th><input id="amount" name="amount" type="number"/></th>
        </tr>
        <tr><th colspan="2">Used for PUT</th></tr>
        <tr>
          <th>Application ID</th>
          <th><input id="id" name="id" /></th>
        </tr>
        <tr>
          <th>Status</th>
          <th>
            <select id="status" name="status" onchange="toggleStatusReasonDisplay()">
              <option value=""></option>
              <option value="0">Submitted</option>
              <option value="1">In Progress</option>
              <option value="2">Accepted</option>
              <option value="3">Denied</option>
            </select>
          </th>
        </tr>
        <tr id="statusReasonDisplay" style="display:none">
          <th>Status Reason</th>
          <th><input id="status_reason" name="status_reason"/></th>
        </tr>
      </table>
    </form>
    <button onclick="postRequest(applicationPostForm, '/applications', applicationsPostResult)">Post Application</button>
    <button onclick="putRequest(applicationPostForm, '/applications', applicationsPostResult)">Put Application</button>
    <button onclick="applicationsPostResult.innerHTML=''">Clear Results</button>
    <script>
      function toggleStatusReasonDisplay() {
        if (document.getElementById("status").value == "3") {
          statusReasonDisplay.style.display = "table-row"
        } else {
          statusReasonDisplay.style.display = "none"
        }
      }
      function toggleLoanAmountDisplay() {
        if (application_type.value == "Loan") {
          LoanAmountToggle.style.display = "table-row"
        } else {
          LoanAmountToggle.style.display = "none"
        }
      }
    </script>
    <br/>
    <br/>
    <div id="applicationsPostResult"></div>
  </div>

  <div style="border:2px solid black;" id = "applicationDelete">
    Delete Card:<br/>
    <form id = "applicationDelForm">
      <table>
          <tr>
              <th>Application Id</th>
              <th><input id="id" name="id"/></th>
          </tr>
      </table>
    </form>
    <button onclick="deleteRequest(applicationDelForm, '/applications', applicationDelResult)">Delete Card</button>
    <button onclick="applicationDelResult.innerHTML=''">Clear Results</button>
    <br/>
    <br/>
    <div id="applicationDelResult">

    </div>
</div>
