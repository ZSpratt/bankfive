import random
import names
import datetime

password = "$2y$12$.NsrLRsMTwdybCjigmIDyOfk/NltLjmWEkUnPIJR0EKBnGwrgIfTy"

category = {
    0 :["Credit"],
    1 :["Other"],
    2: ["Groceries"],
    3: ["Entertainment"],
    4: ["Restaurant & Dining"],
    5: ["Utilities"],
    6: ["Transportation"],
    7: ["Personal"],
    8: ["Insurance"],
    9: ["Health"],
    10: ["Travel"],
    11: ["Miscellaneous"]
}

status = {
    0: ["Submitted"],
    1: ["In Progress"],
    2: ["Accepted"],
    3: ["Denied"]
}

cardtype = {
    0 : ['Visa','Credit'],
    1 : ['Discover','Credit'],
    2 : ['American Express','Credit'],
    3 : ['Mastercard','Credit']
}

profession = {
    0 : ['N/A'],
    1 : ['Teacher'],
    2 : ['Lawyer'],
    3 : ['Plumber'],
    4 : ['Farmer'],
    5 : ['Dentist'],
    6 : ['Software engineer'],
    7 : ['Streamer'],
    8 : ['Independent contractor']
}

discontinue_reason = {
    1 : ["Expired"],
    2 : ["Canceled"],
    3 : ["Lost"],
    4 : ["Stolen"]
}

classification = {
    0 : ["Bad"],
    1 : ['Fair'],
    2 : ['Good'],
    3 : ['Very Good'],
    4 : ['Exceptional']
}

def printTable(tableName, args, data, id=True):
    table = "insert into " + tableName + " " + args + " values\n"
    for i in list(data.keys()):
        #print(str(i) + " : " + str(data[i]))
        if data[i] is not None:
            row = data[i]
            table += "("
            if id:
                 table += str(i) + ","
            for j in range(len(row)):
                if row[j] is None:
                    table += "null"
                elif isinstance(row[j], str):
                    table += '"' + row[j] + '"'
                else:
                    table += str(row[j])
                if (j != len(row) - 1):
                    table += ","
            table += ")"
            if (i != list(data.keys())[-1]):
                table += ","
            else:
                table += ";"
            table += "\n"
    table += "\n"
    return table

cities = ["San Diego", "Sacramento", "Austin", "Redmond", "Puyallup", "Boulder"]
states = ["CA", "WA", "SD", "TX", "NJ", "FL"]

customers = {}
def generateCustomerData():
    data = {}
    usernames = []
    for step in range(10):
        row = []
        gender = random.choice(["male", "female"])
        first_name = names.get_first_name(gender=gender)
        last_name = names.get_last_name()
        username = first_name[0:3] + last_name[0:3]

        if username not in usernames:
            usernames.append(username)

            row.append(username)
            row.append(first_name)
            row.append(last_name)
            row.append(str(random.randint(100, 9999)) + " Street")
            row.append(random.choice(cities))
            row.append(random.choice(states))
            row.append(random.randint(10000, 99999))
            row.append(username + "@email.com")
            row.append(str(random.randint(5550000, 5559999)))

            profession_id = random.choice(list(profession.keys()))
            if profession[profession_id] is None:
                row.append(None)
            else:
                row.append(profession_id)

            row.append(random.choice(list(classification.keys())))
            row.append(random.randint(1000, 9999))
            row.append(random.randint(16, 104))
            row.append(gender)
            row.append("ROLE_USER")
            row.append(True)
            row.append(random.choice([True, False]))
            row.append(password)
            data[step] = row;

    global customers
    customers = data
    return data

cards = {}
loans = {}

applications = {}
def generateApplicationData():
    global applications
    data = {}
    for i in range(150):
        row = []
        applicationBranch = random.choice(["credit", "loan"])
        customer_id = random.choice(list(customers.keys()))
        loancost = random.randint(500, 10000)
        if applicationBranch == "credit":
            row.append("Credit")
            row.append("Credit Application for " + customers[customer_id][1])
        else:
            row.append("Loan " + str(loancost))
            row.append("Loan Application for " + customers[customer_id][1])

        start_date = datetime.date(2019, 1, 1)
        end_date = datetime.date(2021, 12, 31)
        time_between = end_date - start_date
        days_between = time_between.days
        num_days = random.randrange(days_between)
        openDate = start_date + datetime.timedelta(days = num_days)
        row.append(openDate.strftime("%Y-%m-%d"))

        status_id = random.choice([0, 1])
        closed = random.choice([True, False])

        if closed:
            start_date = openDate
            days_between = 1 + int(45 * random.random())
            num_days = random.randrange(days_between)
            closedDate = start_date + datetime.timedelta(days = num_days)

            status_id = random.choice([2, 3])
            row.append(closedDate.strftime("%Y-%m-%d"))
            row.append(status_id)
            if status_id == 2:
                if (applicationBranch == "credit"):
                    addCreditCard(customer_id, closedDate)
                else:
                    addLoan(customer_id, closedDate, loancost)
                row.append(None)
            else:
                row.append(random.choice([None, "User Canceled", "Poor Credit"]))
        else:
            row.append(None)
            row.append(status_id)
            row.append(None)
        row.append(customer_id + 1)
        data[i] = row
    applications = data
    return data

def addCreditCard(customer_id, dateOpened):
    global cards
    row = []

    row.append(customer_id + 1)
    row.append(random.randint(1000000000000000, 9999999999999999))
    row.append(int(str(row[1])[-4:]))
    row.append(random.randint(0, 999))
    row.append(random.choice(list(cardtype.keys())))
    row.append(dateOpened.strftime("%Y-%m-%d"))
    expiration_date = dateOpened + datetime.timedelta(days = 365)
    row.append(dateOpened.strftime("%Y-%m-%d"))
    row.append(0)
    row.append(10000)
    row.append((dateOpened + datetime.timedelta(days = 40)).strftime("%Y-%m-%d"))
    row.append(dateOpened.strftime("%Y-%m-%d"))
    row.append((dateOpened + datetime.timedelta(days = 31)).strftime("%Y-%m-%d"))
    row.append(True)

    cards[len(list(cards.keys()))] = row


def addLoan(customer_id, dateOpened, balance):
    global loans
    row = []

    row.append(customer_id + 1)
    row.append(balance)
    row.append(0.05)
    row.append(dateOpened.strftime("%Y-%m-%d"))
    row.append(customers[customer_id][5])
    row.append(customers[customer_id][4])
    row.append(True)

    loans[len(list(loans.keys()))] = row

transactions = {}
def addTransactions():
    global transactions
    for i in range(500):
        cardPayment = random.choice([True, False])
        customer_id = random.choice(list(customers.keys())) + 1
        row = []
        if cardPayment:
            row.append("Bank 504")
        else:
            row.append(names.get_first_name() + " " + names.get_last_name())
        row.append("Payment to " + row[0])
        amount = random.randint(10, 1000)
        row.append(amount)

        start_date = datetime.date(2018, 1, 1)
        end_date = datetime.date(2020, 12, 31)
        time_between = end_date - start_date
        days_between = time_between.days
        num_days = random.randrange(days_between)
        payDate = start_date + datetime.timedelta(days = num_days)

        row.append(payDate.strftime("%Y-%m-%d"))
        if cardPayment and random.random() > 0.7:
            row.append(False)
        else:
            row.append(True)
        row.append(random.choice(cities))
        row.append(random.choice(states))
        if cardPayment:
            loanPayment = random.choice([True, False])
            if loanPayment:
                customerLoans = []
                row.append(None)
                for loan_id in list(loans.keys()):
                    if loans[loan_id][0] == customer_id:
                        customerLoans.append(loan_id + 1)
                if len(customerLoans) == 0:
                    row.append(None)
                else:
                    row.append(random.choice(customerLoans))
                row.append(customer_id)
            else:
                customerCards = []
                for card_id in list(cards.keys()):
                    if cards[card_id][0] == customer_id:
                        customerCards.append(card_id + 1)
                if len(customerCards) == 0:
                    row.append(None)
                else:
                    row.append(random.choice(customerCards))
                row.append(None)
                row.append(customer_id)
        else:
            customerCards = []
            for card_id in list(cards.keys()):
                if cards[card_id][0] == customer_id:
                    customerCards.append(card_id + 1)
            if len(customerCards) == 0:
                row.append(None)
            else:
                row.append(random.choice(customerCards))
            row.append(None)
            row.append(customer_id)
        if cardPayment:
            row.append(0)
        else:
            category_id = random.choice(list(category.keys()))
            if (category_id == 0):
                category_id += 1;
            row.append(category_id)
        transactions[i] = row
    return transactions

def closeRandomCards():
    global cards
    for card in list(cards.keys()):
        if random.random() > .6:
            reason = random.choice(list(discontinue_reason.keys()))
            cards[card][11] = False
            cards[card].append(reason)
        else:
            cards[card].append(None)

if __name__ == "__main__":
    tables = ""
    tables += printTable("category", "(id, category)", category)
    tables += printTable("status", "(id, status)", status)
    tables += printTable("cardtype", "(id, brand, type)", cardtype)
    tables += printTable("profession", "(id, profession)", profession)
    tables += printTable("discontinue_reason", "(id, reason)", discontinue_reason)
    tables += printTable("classification", "(id, classification)", classification)
    tables += printTable("customer", "(username, first_name, last_name, address_line, city, state, zipcode, email, phone_number, profession_id, classification_id, pin, age, gender, auth, enabled, prospect, password)", generateCustomerData(), id=False)
    tables += printTable("application", "(application_type, description, date_opened, date_closed, status_id, status_reason, applicant_id)", generateApplicationData(), id=False)
    closeRandomCards()
    tables += printTable("card", "(customer_id, card_number, card_number_short, security_code, type, date_opened, expiration_date, card_balance, card_limit, billing_date, bill_cycle_start_date, bill_cycle_end_date, active, discontinue_reason)", cards, id=False)
    tables += printTable("loan", "(customer_id, balance, interest, date_opened, state, city, active)", loans, id=False)
    tables += printTable("transaction", "(merchant_name, description, amount, date, on_time, city, state, card_id, loan_id, customer_id, category_id)", addTransactions(), id=False)
    #print (tables)

    print("Customer Count : " + str(len(customers)))
    print("Application Count : " + str(len(applications)))
    print("Card Count : " + str(len(cards)))
    print("Loan Count : " + str(len(loans)))
    print("Transaction Count : " + str(len(transactions)))

    with open("data.sql", 'w') as file:
        file.write(tables)
