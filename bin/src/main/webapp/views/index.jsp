<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<head>
        <meta charset="ISO-8859-1">
        <title>Bank 504</title>
    </head>

    <body>
        <div id="content">
            <div id="header">
                <h1>Bank 504</h1>
            </div>

            <a href="/">Home</> <br>
            <a href="/page">Page</a> <br>

            <c:if test="${not empty TBody}">
                ${TBody}<br>
                <jsp:include page="${TBody}.jsp" flush="true"/>
            </c:if>
            <c:if test="${empty TBody}">
                Page failed or missing
            </c:if>


        </div>
    </body>
</html>